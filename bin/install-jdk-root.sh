#!/usr/bin/env bash
#
#
#
#

[[ $(id -u) == 0 ]] || {
	echo -e "[!] Você precisa ser o root."
	exit 1
}


source /etc/os-release

# Diretórios que serão usados por este script
readonly DIR_LOCAL_CONFIG='/etc/local-config'
readonly FILE_LOCAL_CONFIG="${DIR_LOCAL_CONFIG}/local-java.sh"



function print_line(){
	echo '-------------------------------------'
}

function print(){
	echo -e " $@"
}

function create_dirs(){

	mkdir -p "$DIR_LOCAL_CONFIG"
	touch "$FILE_LOCAL_CONFIG"

}

function add_fileconfig_in_profile(){
	# Incluir o arquivo de configuração em /etc/profile
	# para ser lido durante o boot.

	local _content_profile="[[ -f ${FILE_LOCAL_CONFIG} ]] && source ${FILE_LOCAL_CONFIG}"
	local _file_profile='/etc/profile'

	touch "$_file_profile"
	grep -q ".*source $FILE_LOCAL_CONFIG" "$_file_profile" && return 0

	print "Configurando $_file_profile"
	echo -e "\n$_content_profile" >> "$_file_profile"

}

function configure_java_home(){
	# Pode existir uma ou mais versões do JAVA no sistema atual
	# esse script irá disponibilizar o JAVA 8 na variável JAVA_HOME.
	#
	print_line
	if [[ ! -x $(command -v javac) ]]; then
		print "[!] Erro: javac não encontrado"
		return 1
	fi

	# Verificar o diretório base do java, se existir mais de uma versão instalada no sistema os
	# arquivos podem ser encontrados em /usr/lib/jvm
	java_dir='/usr/lib/jvm/default-java'

	if [[ ! -d "$java_dir" ]]; then
		print "[!] Erro: diretório não encontrado -> $java_dir"
		return 1
	fi

	unset _content
	readonly _content="export JAVA_HOME=/usr/lib/jvm/default-java"

	# Verificar se a linha de configuração do JAVA_HOME já existe.
	grep -q "$_content" "$FILE_LOCAL_CONFIG" && { 
		# Já configurado anteriormente.
		print "Configuração encontrada em -> $FILE_LOCAL_CONFIG"
		return 0
	}

	print "Incluindo JAVA_HOME em -> $FILE_LOCAL_CONFIG"
	echo -e "$_content" >> "$FILE_LOCAL_CONFIG"

}

function install_jdk_in_linuxmint(){
	print_line
	print "Instalando -> default-jre default-jdk"
	print_line
	sudo apt install default-jre
	sudo apt install default-jdk

}

function install_jdk_in_ubuntu(){
	echo 'Faltar código'
	return 1
}


function install_jdk_base_debian(){
	# https://www.hostinger.com.br/tutoriais/como-instalar-java-no-ubuntu
	#
	#
	print_line
	sudo apt update
	
	case "$ID" in
		ubuntu) ;;
		linuxmint) install_jdk_in_linuxmint;;
		*) ;;
	esac

	configure_java_home
}

function main(){

	create_dirs
	add_fileconfig_in_profile
	
	if [[ -f /etc/debian_version ]]; then
		install_jdk_base_debian
	else
		echo 'Falta código'
		return 1
	fi

}

main "$@"
