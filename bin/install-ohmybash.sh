#!/usr/bin/env bash
#
# https://github.com/ohmybash/oh-my-bash
# https://raw.githubusercontent.com/ohmybash/oh-my-bash/master/tools/install.sh
# bash -c "$(curl -fsSL https://raw.githubusercontent.com/ohmybash/oh-my-bash/master/tools/install.sh)"

this_file=$(readlink -f "$0")
this_dir=$(dirname "$this_file")

unset SHELL_LIBS
readonly SHELL_LIBS="${this_dir}/shell-libs"

source "${this_dir}/shell-libs/__init__.sh" || exit 1
setDirsUser

readonly dir_temp="$HOME/TMP"
readonly dir_unpack="$dir_temp/unpack"
readonly dir_download="$HOME/Downloads/appcli-downloads"
readonly dir_backups=~/bashrc-backups
readonly dir_bash_themes=~/.bash/themes/
readonly INSTALL_DIR="${HOME}/opt/ohmybash"

URL='https://github.com/ohmybash/oh-my-bash/archive/refs/heads/master.zip'
URL_INSTALLER='https://raw.githubusercontent.com/ohmybash/oh-my-bash/master/tools/install.sh'
FILECACHE_OHMYBASH_PROJECT="$dir_download/ohmybash.zip"
FILECACHE_INSTALLER="$dir_download/ohmybash-install.sh"

function create_dirs(){
	mkdir -p "$dir_temp"
	mkdir -p "$dir_unpack"
	mkdir -p "$dir_download"
	mkdir -p "$dir_backups"
	mkdir -p "$dir_bash_themes"
	mkdir -p "$INSTALL_DIR"
}

function run_backup_bashrc(){
	local file_backup="$dir_backups/bashrc-ohmybash-$(date +%Y%m%d%H%M%S)"
	cp ~/.bashrc "$file_backup"
}

function downloadOhmybash(){
	download "$URL" "$FILECACHE_OHMYBASH_PROJECT" 
	download "$URL_INSTALLER" "$FILECACHE_INSTALLER"
}

function install_themes(){
	
	ASSUME_YES=true
	removeFiles "$dir_unpack"
	create_dirs
	unpackArchive "$FILECACHE_OHMYBASH_PROJECT" "$dir_unpack"
	printLine
	print "Instalando themas para ohmybash"
	cd "$dir_unpack"
	mv $(ls -d oh*) ohmybash
	cd ohmybash/themes || return 1
	cp -R . "${dir_bash_themes}/"
	print "Configurando thema [agnoster]"
	sed -i "s|OSH_THEME=.*|OSH_THEME=agnoster|g" "$HOME/.bashrc"
	return "$?"
}

function install_ohmybash(){
	downloadOhmybash || return 1
	run_backup_bashrc
	install_themes || return 1
	printLine
	chmod +x "$FILECACHE_INSTALLER"
	"$FILECACHE_INSTALLER" 
	return 0
}

function uninstall_ohmybash(){
	removeFiles "${HOME}/.oh-my-bash"
}

function main(){
	create_dirs

	case "$1" in
		-i|--install) install_ohmybash;;
		-u|--uninstall) uninstall_ohmybash;;
		*) install_ohmybash;;
	esac
}

main "$@"