#!/usr/bin/env bash
#
#
# https://www.clubedolinux.com.br/como-configurar-um-ambiente-de-desenvolvimento-c-c-no-linux/
#
#

unset this_file
unset this_dir
unset SHELL_LIBS

readonly this_file=$(readlink -f "$0")
readonly this_dir=$(dirname "$this_file")
readonly SHELL_LIBS="${this_dir}/shell-libs"

source "${SHELL_LIBS}/__init__.sh"
source /etc/os-release

function create_dirs(){
	echo
}

function install_ides(){
	echo
}

function install_cpp_comp_debian(){
	printLine
	sudoCommand apt install -y build-essential
}

function install_cpp_comp(){
	
	if [[ -f '/etc/debian_version' ]]; then
		install_cpp_comp_debian
	fi

	sudo apt install codeblocks -y

}

function main(){
	
	case "$1" in
		*) install_cpp_comp;;
	esac
}

main "$@"

