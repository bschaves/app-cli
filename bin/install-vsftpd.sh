#!/usr/bin/env bash
#
#

[[ $(id --user) == 0 ]] || {
	echo -e "Erro: você precisa ser o 'root'."
	exit 1
}


readonly CONFIG_FILE_VSFTPD='/etc/vsftpd.conf'
readonly BACKUP_CONFIG_FILE='/etc/vsftpd.conf.backup'

source /etc/os-release || exit 1

function print_line(){
	echo '-------------------------------------'
}

function exit_app(){
	print_line
	if [[ "$1" == 0 ]]; then
		echo -e "[+] Programa finalizado com status $1"
		return 0
	else
		echo -e "[!] ERRO => Programa finalizado com status $1"
	fi
	return "$1"
}

function backup_config_file(){
	[[ -f "$BACKUP_CONFIG_FILE" ]] && return 0
	print_line
	echo -e "Criando backup => $BACKUP_CONFIG_FILE"
	cp "$CONFIG_FILE_VSFTPD" "$BACKUP_CONFIG_FILE"
}

function configure_file_config(){
	backup_config_file
	print_line
	echo -e "Configurando arquivo => $CONFIG_FILE_VSFTPD"

	{
		echo 'listen=NO';
		echo 'listen_ipv6=YES';
		echo 'local_enable=YES';
		echo 'use_localtime=YES';
		echo 'secure_chroot_dir=/var/run/vsftpd/empty';
		echo 'pam_service_name=vsftpd';
		echo 'rsa_cert_file=/etc/ssl/certs/ssl-cert-snakeoil.pem';
		echo 'rsa_private_key_file=/etc/ssl/private/ssl-cert-snakeoil.key';
		echo 'ssl_enable=NO';
		echo 'local_root=/';
		echo 'write_enable=NO';
	} > "$CONFIG_FILE_VSFTPD"

	print_line
	cat "$CONFIG_FILE_VSFTPD"
	print_line

}

function restart_vsftpd(){
	#/bin/systemctl restart vsftpd.service
	# service vsftpd restart
	systemctl enable vsftpd
	service vsftpd stop
	service vsftpd start
	service vsftpd status
}

function install_vsftpd(){
	
	case "$ID" in
		debian|ubuntu|linuxmint) sudo apt install vsftpd;;
		*) echo -e "ERRO: seu sistema não é suportado."; return 1;;
	esac
}

function main(){
	install_vsftpd || return 1
	configure_file_config
	restart_vsftpd
}

main "$@"
exit_app "$?"