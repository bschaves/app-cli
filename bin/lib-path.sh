# Proteção contra o root.
if [[ $(id --user) == 0 ]]; then
	echo '[!] Erro -> você não pode ser o root.'
	exit 1
fi

__version__='2024-06-08'
__appname__='add-path'
this_file_config_paths=~/add-local-paths.sh
silent=false # Não exibe mensagens no stdout.
file_shell_rc=false
readonly user_shell=$(basename "$SHELL") # bash|zsh

if [[ $(basename "$SHELL") == 'bash' ]]; then
	file_shell_rc=~/.bashrc
	source "$file_shell_rc"
elif [[ $(basename "$SHELL") == 'zsh' ]]; then
	file_shell_rc=~/.zshrc
	source "$file_shell_rc"
fi

function show_text(){
	[[ "$silent" == true ]] && return 0
	echo -e "$@"
}

function add_file_with_paths_in_home(){
	# Incluir o arquivo de configuração deste script para ser lido no login do usuário.
	touch "$this_file_config_paths"
	grep -q "^#!/usr/bin/env.*" "$this_file_config_paths" && return 0

	#sed -i '1s/^/\#!\/usr\/bin\/env bash/' "$this_file_config_paths"
	if [[ $(basename "$SHELL") == 'bash' ]]; then
		echo '#!/usr/bin/env bash' > "$this_file_config_paths"
	elif [[ $(basename "$SHELL") == 'zsh' ]]; then
		echo '#!/usr/bin/env zsh' > "$this_file_config_paths"
	fi
	echo -e "\n" >> "$this_file_config_paths"
	chmod +x "$this_file_config_paths"
	return 0
}

function configure(){
	show_text "[+] Configurando SHELL [$user_shell]"
	add_dir_in_path_user "${HOME}/bin" || return 1
	return 0
}

function add_dir_in_path_user(){
	# Incluir um diretório no arquivo de configuração para ser adicionado na variável
	# PATH durante o login do usuário.
	local newDirInPath="$1"
	local _line_configuration="echo -e \$PATH | grep -q $newDirInPath || export PATH=\$PATH:$newDirInPath"

	# Verificar se o arquivo de confiração existe na pasta do usuário, e incluir se necessário.
	add_file_with_paths_in_home

	# Verificar se a leitura do arquivo de configuração está inclusa em ~/.bashrc|~/.zshrc
	grep -q ".*source ${this_file_config_paths}" "$file_shell_rc" || {
		show_text "[+] Incluindo configuração no arquivo $file_shell_rc"
		echo -e "[[ -f $this_file_config_paths ]] && source ${this_file_config_paths}" >> "$file_shell_rc"
	}

	# Verificar se o diretório existe.
	if [[ ! -d "$newDirInPath" ]]; then 
		show_text "[!] o diretório não existe -> $newDirInPath"
		return 1
	fi

	# Verificar se o diretório informado em $1 já existe no arquivo de configuração.
	grep -q "^export PATH.*$PATH:$newDirInPath" "$this_file_config_paths" && return 0
	grep -q "$_line_configuration" "$this_file_config_paths" && return 0
	dir_exists_in_path "$newDirInPath" && return 0

	# Adicionar o deretório $1 no arquivo de configuração.
	show_text "[+] Adicionando [$newDirInPath] em PATH"
	echo -e "$_line_configuration" >> "$this_file_config_paths"
	return 0
}


function check_dir(){
	# se o diretório informado em $1 existir na variável PATH retorna 0, se não, retorna 1.

	[[ -z "$1" ]] && { show_text "[!] Parâmetro incorreto detectatdo em -> [check_dir]"; return 1; }
	[[ ! -d "$1" ]] && { show_text "[!] Erro -> [$1] não é um diretório"; return 1; }

	# Verificar se $1 já existe em PATH.
	dir_exists_in_path "$1" && return 0
	return 1
}

function dir_exists_in_path(){
	# Verificar se um diretório existe em PATH.
	echo -e "$PATH" | grep -q "$1" && return 0
	return 1
}

function usage(){

	echo -e "  $__appname__ V$__version__\n"
	echo -e " Use:\n"
	echo -e " -h|--help              Exibe ajuda"
	echo -e " -s|--silent            Não exibe mensagens no stdout"
	echo -e " -d|--dir <path-dir>    Inclui um diretório no arquivo ~/.bashrc"
	echo -e " --checkdir <dir>       Verifica se um diretório está na variável PATH"
	echo
}

function main(){

	if [[ -z "$1" ]]; then
		usage
		return 0
	fi
	
	for arg in "$@"; do
		if [[ "$arg" == '-s' ]] || [[ "$arg" == '--silente' ]]; then
			silent=true
		fi
	done

	case "$1" in
		-h|--help) usage; return 0;;
		-d|--dir) shift; add_dir_in_path_user "$1"; return $?;;
		-c|--configure) configure;;
		--checkdir) shift; check_dir "$@"; return $?;;
		*) 
		echo -e "[!] Parâmetros incorretos detectados."
		usage
		return 1	
			;;
	esac
}

main "$@"


