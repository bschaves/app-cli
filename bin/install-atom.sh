#!/usr/bin/env bash
#
#
# https://diolinux.com.br/sistemas-operacionais/como-programar-em-cc-no-ubuntu.html
#


unset this_file
unset this_dir
unset SHELL_LIBS

readonly this_file=$(readlink -f "$0")
readonly this_dir=$(dirname "$this_file")
readonly SHELL_LIBS="${this_dir}/shell-libs"

source "${SHELL_LIBS}/__init__.sh"


readonly temp_dir=$(create_user_temp_dir)

readonly URL_ATOM='https://github.com/atom/atom/releases/download/v1.58.0/atom-amd64.tar.gz'
readonly DIR_DOWNLOADS="${HOME}/Downloads/appcli-downloads"
readonly FILECACHE_ATOM="${DIR_DOWNLOADS}/atom-amd64.tar.gz"
readonly UNPACK_DIR="${temp_dir}/unpack"
readonly INSTALL_DIR_ATOM="${HOME}/opt/atom-x64"
readonly FILE_DESKTOP_ENTRY='atom'
readonly SCRIPT_EXECUTABLE_ATOM="$HOME/bin/atom"

function download_atom(){
	download "$URL_ATOM" "$FILECACHE_ATOM"
}

function create_dirs(){
	mkdir -p "$DIR_DOWNLOADS"
	mkdir -p "$UNPACK_DIR"
	mkdir -p "$INSTALL_DIR_ATOM"
}

function create_script_atom(){
	
	echo '#!/usr/bin/env bash' > "$SCRIPT_EXECUTABLE_ATOM"
	{
		echo -e "\n${INSTALL_DIR_ATOM}/atom \$@";
	} >> "$SCRIPT_EXECUTABLE_ATOM"

	chmod +x "$SCRIPT_EXECUTABLE_ATOM"
	cd "$this_dir"
	./add-path --dir "$HOME/bin"
}

function install_atom(){
	download_atom
	ASSUME_YES=true
	removeFiles "$UNPACK_DIR"
	create_dirs
	unpackArchive "$FILECACHE_ATOM" "$UNPACK_DIR"
	cd "$UNPACK_DIR"
	
	print "Instalando atom"
	mv $(ls -d atom*) atom
	cd atom
	cp -R -u * "$INSTALL_DIR_ATOM/"
	removeFiles "$UNPACK_DIR"
	cd "$INSTALL_DIR_ATOM"
	chmod +x atom
	create_script_atom
	
	desktop_info_atom=(
			"Name=Atom"
            "Comment=Code Editing."
            "GenericName=Text Editor"
            "Exec=$SCRIPT_EXECUTABLE_ATOM"
            "Icon=$INSTALL_DIR_ATOM/atom.png"
            "Type=Application"
            "StartupNotify=false"
            "Categories=Utility;TextEditor;Development;IDE;"
            "Keywords=atom;"
	)

	addDesktopEntry "$FILE_DESKTOP_ENTRY" "${desktop_info_atom[@]}"
	return 0;
}

function uninstall_atom(){
	removeFiles "$INSTALL_DIR_ATOM"
	removeFiles "$FILE_DESKTOP_ENTRY"
	removeFiles "$SCRIPT_EXECUTABLE_ATOM"
}

function main(){

	create_dirs
	
	case "$1" in
		-i|--install) install_atom;;
		-u|--uninstall) uninstall_atom;;
		*) install_atom;;
	esac
}

main "$@"