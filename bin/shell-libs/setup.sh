#!/usr/bin/env bash
#

file_setup=$(readlink -f $0)
dir_project=$(dirname $file_setup)
force_install=false

export SHELL_LIBS="$dir_project"
source "${dir_project}"/__init__.sh || exit 1

if isRoot; then
    print_erro 'Você não pode ser o "root"'
    exit 1
fi

setDirsUser
PREFIX=~/lib
CONFIG_FILE=~/shell-libs.conf
SCRIPT_ADD_PATH="${DIR_BIN}/add-path"
mkdir -p "$PREFIX"
mkdir -p "${DIR_BIN}"

INSTALL_DIR="${PREFIX}/shell-libs"

function create_local_dirs(){
    mkdir -p "$INSTALL_DIR"
}

function copyFilesShellLibs(){

    if [[ -d "$INSTALL_DIR" ]]; then
        if [[ "$force_install" == false ]]; then
            echo -e "INSTALL_DIR já instalado em ${INSTALL_DIR} "
            echo -e "use: -u|--uninstall para desistalar a versão anterior"
            return 0
        fi
    fi

    print "[INSTALANDO] $__appname__ V${__version__}"
    [[ -d "$INSTALL_DIR" ]] && rm -rf "${INSTALL_DIR}" # Desinstalar versão anterior
    create_local_dirs
    cd "$dir_project" || return 1
    cp -R * "${INSTALL_DIR}"/ || return 1
    print "[OK] shell-libs instalado em -> $INSTALL_DIR"
    return 0
}

function add_script_path()
{
    [[ $(id -u) == 0 ]] && return 1
    cd "$dir_project"
    cp -u scripts/add-path "$SCRIPT_ADD_PATH"
    chmod +x "$SCRIPT_ADD_PATH"
    "$SCRIPT_ADD_PATH" --dir "$DIR_BIN"
}

function add_file_config()
{
    # Adicionar o arquivo de configuração.
    [[ $(id -u) == 0 ]] && return 1

    echo -e "export SHELL_LIBS=${INSTALL_DIR}" > ~/shell-libs.conf
    grep -q "^source $CONFIG_FILE" ~/.bashrc && return
    echo -e "source $CONFIG_FILE" >> ~/.bashrc
}

function uninstallShellLibs(){

    if [[ ! -d "$INSTALL_DIR" ]]; then
        print_erro "Diretório não encontrado -> ${INSTALL_DIR}"
        return 1
    fi

    print "Desinstalando shell-libs"
    [[ -d "$INSTALL_DIR" ]] && rm -rf "${INSTALL_DIR}"
    echo -e "" > "$CONFIG_FILE"
}

function installShellLibs(){
    copyFilesShellLibs
    add_file_config
    add_script_path
}

function usage(){

    echo -e " $this_appname V$this_version\n"
    echo -e " Use:"
    echo -e " -h|--help        Exibe ajuda"
    echo -e " -u|--uninstall   Desinstalar o shell-libs"
    echo -e " -f|--force       Sobreescre uma versão anterior pela versão desse pacote"


}

function main(){

    if [[ "$1" == '-f' ]] || [[ "$1" == '--force' ]]; then force_install=true; fi

    case "$1" in
        -h|--help) usage; return;;
        -u|--uninstall) uninstallShellLibs; return;;
        *) installShellLibs;;
    esac
}

main $@





