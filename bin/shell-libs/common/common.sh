#!/usr/bin/env bash
#
#
#=============================================================#
# + print_text - Imprimir textos com formatação e cores.
#=============================================================#
#
# DEPENDÊNCIAS:
#	sed
# 	awk
#   tput
#	ps
#
#=============================================================#
# SED
#=============================================================#
# find ./ -type f -name '*.sh' -exec sed 's/print_erro/print_erro/g' "{}" \;
#
#=============================================================#
# FUNÇÕES
#=============================================================#
# - print_erro
# - printErro
# - printInfo
# - printLine
# - print_erro_param
# - print
# - replace
# - upper
# - lower
# - date
#
#
#=============================================================#
# REFERÊNCIAS
#=============================================================#
# https://www.zentica-global.com/pt/zentica-blog/ver/como-cortar-string-em-bash---linux-hint-60739de9bdd55
# https://giovannireisnunes.wordpress.com/2017/06/02/manipulacao-de-strings-em-bash
# https://www.shellscriptx.com/2016/12/utilizando-expansao-de-variaveis.html
# https://solitudelab.wordpress.com/2017/12/21/shell-script-operadores-de-comparacao-e-entrada-de-dados/
#


#=============================================================#
# Cores.
#=============================================================#
# Regular Text
CRed="\033[0;31m"
CGreen="\033[0;32m"
CYellow="\033[0;33m"
CBlue="\033[0;34m"
CPrurple="\033[0;35m"
CCyan="\033[0;36m"
CGray="\033[0;37m"
CWhite="\033[0;37m"
CReset="\033[0m"

# [S] - Strong text (bold)
CSRed="\033[1;31m"
CSGreen="\033[1;32m"
CSYellow="\033[1;33m"
CSBlue="\033[1;34m"
CSPurple="\033[1;35m"
CSCyan="\033[1;36m"
CSGray="\033[1;37m"
CSWhite="\033[1;37m"

# [D] - Dark text
CDRed="\033[2;31m"
CDGreen="\033[2;32m"
CDYellow="\033[2;33m"
CDBlue="\033[2;34m"
CDPurple="\033[2;35m"
CDCyan="\033[2;36m"
CDGray="\033[2;37m"
CDWhite="\033[2;37m"

# [I] Italicized text
CIRed="\033[3;31m"
CIGreen="\033[3;32m"
CIYellow="\033[3;33m"
CIBlue="\033[3;34m"
CIPurple="\033[3;35m"
CICyan="\033[3;36m"
CIGray="\033[3;37m"
CIWhite="\033[3;37m"

# [U] - Underlined text
CURed="\033[4;31m"
CUGreen="\033[4;32m"
CUYellow="\033[4;33m"
CUBlue="\033[4;34m"
CUPurple="\033[4;35m"
CUCyan="\033[4;36m"
CUGray="\033[4;37m"
CUWhite="\033[4;37m"

# [B] - Blinking text
CBRed="\033[5;31m"
CBGreen="\033[5;32m"
CBYellow="\033[5;33m"
CBBlue="\033[5;34m"
CBPurple="\033[5;35m"
CBCyan="\033[5;36m"
CBGray="\033[5;37m"
CBWhite="\033[5;37m"

# Calcular os pixel da janela de terminal.
export terminal_columns=$(tput cols)

function set_terminal_columns(){
	# setar/atualizar o tamanho das colunas do terminal.
	export terminal_columns=$(tput cols)	
}

function get_terminal_size(){
	set_terminal_columns
	echo -e "$(tput cols)"
}

printLine() # -> None
{
    # + arg 1 = caracter a ser impresso no terminal (=, +, *, ~, -, #, ...).
    # + type arg 1 = string.
    #
    # Imprime um caracter que ocupa todas as colunas do terminal, o padrão é "-".
    #
    if [[ -z $1 ]]; then
	    printf "%$(tput cols)s\n" | tr ' ' '-'
	else
	    printf "%$(tput cols)s\n" | tr ' ' "$1"
	fi
}


function print_erro(){
    # args = number + string 
    # Imprime uma mensagem de erro personalizada na cor padrão do terminal.
    printLine
	echo -e "[ERRO] -> $@"
}

function print_erro_param(){
	# Função para exibir erro genérico quando outra função retorna erro, por parâmetros incorretos
	# na linha de comando.
	#
	#print_erro "$1 - parâmetros incorretos detectados."
	sred "[$1] => parâmetros incorretos detectados."
}


printErro()
{
    # Exibe uma mensagem personalizada de erro em vermelho.
	if [[ -z $1 ]]; then
		echo -e "${CRed}ERRO${CReset}"
	else
		echo -e "${CRed} [!] ERRO ->${CReset} $@"
	fi
}

printInfo()
{
	echo -e "${CYellow}[INFO] ${CReset}$@"
}


print()
{
    echo -e "[+] $@"
}


msg()
{
	printLine '='
	echo -e " $@"
	printLine
}

red()
{
	echo -e "${CRed}[!] ${CReset}$@"
}

green()
{
	echo -e "${CGreen}[+] ${CReset}$@"
}

yellow()
{
	echo -e "${CSYellow} $@${CReset}"
}

blue()
{
	echo -e "${CBlue} + ${CReset}$@"
}

white()
{
	echo -e "${CWhite} + ${CReset}$@"
}

sred()
{
	echo -e "${CSRed}$@${CReset}"
}

sgreen()
{
	echo -e "${CSGreen}$@${CReset}"
}

syellow()
{
	echo -e "${CSYellow}$@${CReset}"
}

sblue()
{
	echo -e "${CSBlue}$@${CReset}"
}

#=============================================================#
# STRINGS
#=============================================================#

function upper()
{
    # arg 1 = string/texto
    # type arg 1 = string
    #

	local _string="$1"
	echo -e "${_string^^}"
}


function lower()
{
    # arg 1 = string/texto
    # type arg 1 = string
    #
	local _string="$1"
	echo -e "${_string,,}"
}

function replace(){
    # arg 1 = texto original a ser substituido
    # arg 2 = corrência que deseja substituir no texto
    # arg 3 = valor final da ocorrência.
    # type args = string
    #
    # SUBSTITUI $2 por $3 no texto original $1.
    local _string="$1"
    echo "$_string" | sed "s/$2/$3/g"
}

#=============================================================#
# LOOP PID
#=============================================================#


question()
{
	# Indagar o usuário com uma pergunta em que a resposta deve ser do tipo SIM ou NÃO (s/n)
	# esta função automatiza as indagações.
	#
	#   se o usuário teclar "s|S" -----------------> retornar 0  
	#   se o usuário teclar "n|N" ou nada ---------> retornar 1
	#
	# $1 = Mensagem a ser exibida para o usuário, a resposta deve ser SIM ou NÃO (s/n).
    # o tempo de espera pela resposta é de 15s.
	#
    # O usuário NÃO será indagado, caso a variável $AssumeYes tiver valor igual "True".
    # nesse caso essa função SEMPRE irá retornar 0, sem perguntar nada.

	[[ "$AssumeYes" == 'True' ]] && return 0
		
	echo -ne "$@ ${CGreen}s${CReset}/${CRed}N${CReset}?: "
	read -t 15 -n 1 yesno
	echo ' '

	case "${yesno,,}" in
		s|y) return 0;;
		*) printf "${CRed}Abortando${CReset}\n"; return 1;;
	esac
}

function is_process(){
	# $1 = número de um processo do sistema
	# se $1 existir -> retorna 0
	# se não --------> retorna 1
	
	[[ -z $1 ]] && return 1
	[[ $(ps aux | grep -m 1 "$1" | awk '{print $2}') == "$1" ]] && return 0
	return 1
}

function get_process_cli(){
	local _cli=$(ps aux | grep -m 1 "$1" | awk '{print $11}')
	echo -e "$_cli"
}

loopPid()
{
    # arg 1 = int - deve ser um pid em execução.
    # arg 2 = string - texto a ser exibido no terminal em quanto o processe estiver em execução.
    # type arg 1 = int
    # type arg 2 = string/text
    #
	# Função para executar um loop enquanto determinado processo (PID) do sistema está em 
    # execução. Por exemplo um outro processo de instalação de pacotes, como o "apt install" ou 
    # "pacman install" por exemplo, o pid deve ser passado como argumento $1 da função. Enquanto 
    # esse processo existir o loop irá bloquar a execução deste script, que será retomada assim que o
	# processo informado for encerrado.
	local array_chars=('\' '|' '/' '-')
	local num_char='0'
	local Pid="$1"
	local MensageText=''

	[[ $2 ]] && MensageText="$2"
    
    # Preencher a linha atual com espaços em branco.
	echo -ne "$(printf "%-$(tput cols)s" | tr ' ' ' ')\r" 

	while true; do
        # Todos os processos em execução no sistema.
		# ALL_PROCS=$(ps aux) 

        # Verificar se o processo passado em $1 está em execução no sistema.
		#[[ $(echo -e "$ALL_PROCS" | grep -m 1 "$Pid" | awk '{print $2}') != "$Pid" ]] && break
		! is_process "$Pid" && break

		Char="${array_chars[$num_char]}"		
		echo -ne "$MensageText ${CYellow}[${Char}]${CReset}\r" # $(date +%H:%M:%S)
		sleep 0.15
		
		num_char="$(($num_char+1))"
		[[ "$num_char" == '4' ]] && num_char='0'
	done
	#echo -e "$MensageText [${Char}] OK"
	echo -e "$MensageText ${CGreen}[${Char}]${CReset} "	
}


loop_line()
{
    # arg 1 (int) = número de um PID em execução no sistema.
    # arg 2 (string) = texto a ser exibido enquanto um pid está em execução
	#
	# impede a execução de código enquanto determinado PID estiver em execução.
	local num=1
	local max='6'
	local med='3'
	local direction='toend'
	local sys_pid="$1"
	local show_terminal_msg=" [$(date +%H:%M:%S)] Aguardando PID $sys_pid"
	local chars=(
		'[<=>     ]'
		'[ <=>    ]'
		'[  <=>   ]'
		'[   <=>  ]'
		'[    <=> ]'
		'[     <=>]'
		)

	if [[ $(get_terminal_size) -ge 60 ]]; then
		local chars=(
		'[<=>         ]'
		'[ <=>        ]'
		'[  <=>       ]'
		'[   <=>      ]'
		'[    <=>     ]'
		'[     <=>    ]'
		'[      <=>   ]'
		'[       <=>  ]'
		'[        <=> ]'
		'[         <=>]'
		)
		local max='10'
		local med='5'
	fi

	
	
	while true; do
		! is_process "$sys_pid" && break
		current_char="${chars[$num]}"

		echo -ne "$show_terminal_msg $current_char\r"; sleep 0.12

		if [[ "$direction" == 'toend' ]]; then
			if [[ "$num" == "$max" ]]; then
				direction='tostart' # voltar do final para o começo
				num="$(($num-1))"
			else
				num="$(($num+1))"
			fi
		elif [[ "$direction" == 'tostart' ]]; then
			if [[ "$num" == '0' ]]; then
				direction='toend'
				num="$(($num+1))"
			else
				num="$(($num-1))"
			fi
		fi

	done
	echo -e "$show_terminal_msg $current_char "	
}


waitPid()
{
    # arg 1 (int) = número de um PID em execução no sistema.
    # arg 2 (string) = texto a ser exibido enquanto um pid está em execução
	#
	# impede a execução de código enquanto determinado PID estiver em execução.
	local array_chars=('\' '|' '/' '-')
	local num_char='0'
	local Pid="$1"

	if [[ -z $2 ]]; then
		local show_terminal_msg="Aguardando processo com pid [$Pid] finalizar"
	else
		local show_terminal_msg="$2"
	fi

	while true; do
		! is_process "$Pid" && break

		Char="${array_chars[$num_char]}"		
		# $(date +%H:%M:%S)
		echo -ne " [$(date +%H:%M:%S)] $show_terminal_msg [${Char}]\r"
		sleep 0.1
		num_char="$(($num_char+1))"
		[[ "$num_char" == '4' ]] && num_char='0'
	done
	echo -e " [$(date +%H:%M:%S)] -> $show_terminal_msg ${CYellow}finalizado${CReset} [${Char}]"	
}
