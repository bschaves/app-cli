#!/usr/bin/env bash
#
# cut
# file
# id
# whoami
# awk
# unzip
# tar
# gpg
# shasum
#
#

export ASSUME_YES=false

KERNEL_TYPE=$(uname -s)
[[ -f /etc/os-release ]] && source /etc/os-release

function setStatusErro(){
	echo '1' > $SHELL_STATUS_FILE
}

function getDistroVersion() # -> number
{
    # Obter o número de versão da distro Linux
    grep 'VERSION_ID=' /etc/os-release | sed 's/VERSION_ID=//g;s/"//g'
}

function isDebian(){
    
    grep -q -m 1 ^'ID=debian' /etc/os-release || return 1
    [[ -f /etc/debian_version ]] || return 1
    return 0
}

function isLinuxmint(){
    
    grep -q -m 1 ^'ID=ubuntu' /etc/os-release || return 1
    grep -q -m 1 ^'ID_LIKE=debian' /etc/os-release && return 1
    [[ -f /etc/debian_version ]] || return 1
    return 0
}

function isUbuntu(){
    
    grep -q -m 1 ^'ID=linuxmint' /etc/os-release || return 1
    [[ -f /etc/debian_version ]] || return 1
    return 0
}

function isLinuxmintDebianEdition(){
    # 
    grep -q -m 1 ^'NAME="LMDE"' /etc/os-release || return 1
    grep -q -m 1 ^'ID_LIKE=debian' /etc/os-release || return 1
    grep -q -m 1 ^'ID=linuxmint' /etc/os-release || return 1
    return 0
}

function isFile(){
	# $1 = arquivo para verificar.
	[[ -f "$1" ]] && return 0
	return 1
}


function isDir(){
    # $1 = diretório para verificar.
	[[ -d "$1" ]] && return 0
	return 1
}

isAdmin(){
	# Verifica se o usuário atual é administrador.
	[[ $(whoami) == 'root' ]] && return 0

	printf "Autênticação necessária para prosseguir "
	if [[ $(sudo id -u) == 0 ]]; then
		printf "OK\n"
		return 0
	else
		printErro "ERRO"
		return 1
	fi
}

function isRoot()
{
	# Verifica se o usuário atual é o root.
	[[ $(id -u) == 0 ]] && return 0
	return 1
}


function isExecutable()
{
	# Função para verificar se um executável existe no PATH do sistema.
	if [[ -x $(command -v "$1" 2> /dev/null) ]]; then
		return 0
	else
		return 1
	fi
}


function _remove_files_root(){
    # args = string/diretórios/arquivos.
    # type args = string.
    #
	
	
	[[ -z $1 ]] && return 1

	echo -e "Deseja ${CRed}deletar${CReset} os seguintes arquivos/diretórios?: "
	for _dir in "${@}"; do echo -e "$_dir"; done
	
	question "" || return 1
    printLine

	while [[ $1 ]]; do	
        if [[ $(dirname $1) == '/' ]]; then
            echo -e "PULANDO ... $1"
            shift
            continue
        fi
	
		cd $(dirname "$1")
		echo -e "Removendo ... $1"
		#rm -rf "$1"
		shift
	done
}


function removeFiles()
{
	# Apaga arquivos e diretórios recursivamente.
    # args = string/diretórios/arquivos.
    # type args = string.
    #
	
	
	[[ -z $1 ]] && return 1

	if [[ "$ASSUME_YES" == false ]]; then
		echo -e "Deseja ${CRed}deletar${CReset} os seguintes arquivos/diretórios?: "
		for _dir in "${@}"; do echo -e "$_dir"; done
		question "" || return 1
	fi

	while [[ $1 ]]; do	
        
        #if [[ $(dirname $1) == "$HOME" ]]; then
        #    echo -e "PULANDO ... $1"
        #    shift
        #    continue
        #fi
	
 		if [[ ! -f "$1" ]] && [[ ! -d "$1" ]]; then
 			printErro "O arquivo/diretório não existe -> $1"
 			shift
 			continue
 		fi


        if [[ ! -w "$1" ]]; then
            yellow "[PULANDO] => você não tem permissão de escrita [w] em => $1"
            shift
            continue
        fi

		cd $(dirname "$1")
		echo -e "[APAGANDO] => $1"
		rm -rf "$1"
		shift
	done
}


function fileInfo(){
	# Retorna informções de um arquivo com base no comando file.
	[[ ! -f $1 ]] && {
		print_erro_param "fileInfo"
		return 1
	}

	file "$1" | cut -d ' ' -f 2
}


sudoCommand()
{
	# Função para executar comandos com o "sudo" e retornar '0' ou '1'.
	echo -e "Executando => sudo $@"
	if sudo "$@"; then return 0; fi
	
	print_erro "sudo $@"
	return 1
}


function unpackArchive()
{
	# Descomprime arquivos mostrando um loop no stdout.
	# $1 = arquivo
	# $2 = diretório para descomprimir (OPCIONAL)

	local _unpackDir=$(pwd) # Local onde os arquivos serão descomprimidos.

	if ! isFile "$1"; then
		print_erro_param 'unpack_archive'
		return 1
	fi


	if [[ -d "$2" ]]; then _unpackDir="$2"; fi


	[[ ! -w "$_unpackDir" ]] && { 
		red "Você não tem permissão de escrita [-w] em ... $_unpackDir"
		return 1	
	}

	local path_file="$1"
	unpack "$path_file" "$_unpackDir" 1> $SHELL_DEVICE_FILE 2>&1 &
	
	# echo -e "$(date +%H:%M:%S)"
	loopPid "$!" "[DESCOMPACTANDO] -> $(basename $path_file)"
	[[ $(cat $SHELL_STATUS_FILE) == 0 ]] && return 0
	print_erro "unpack_archive"
	return 1
}



function unpack()
{
	# Descomprimir vários tipos de arquivos.
	#
	# $1 = arquivo a ser descomprimido - (obrigatório)
	# $2 = diretório de saída - (opcional)
	
	local _unpackDir=$(pwd)

	if ! isFile "$1"; then
		print_erro_param 'unpack_archive'
		return 1
	fi


	if [[ -d $2 ]]; then _unpackDir="$2"; fi


	[[ ! -w "$_unpackDir" ]] && { 
		red "Você não tem permissão de escrita [-w] em ... $_unpackDir"
		return 1	
	}

	# Obter o tipo de arquivo
	path_file="$1"
	extension_file=$(fileInfo $1)

	
	# Calcular o tamanho do arquivo
	# local len_file=$(du -hs $path_file | awk '{print $1}')
	echo -ne "Descompactando ... $(basename $1) "

	# Descomprimir de acordo com cada extensão de arquivo.	
	if [[ "$extension_file" == 'gzip' ]]; then
		tar -zxvf "$path_file" -C "$_unpackDir" 1> /dev/null 2>&1
	elif [[ "$extension_file" == 'bzip2' ]]; then
		tar -jxvf "$path_file" -C "$_unpackDir" 1> /dev/null 2>&1
	elif [[ "$extension_file" == 'XZ' ]]; then
		tar -Jxf "$path_file" -C "$_unpackDir" 1> /dev/null 2>&1
	elif [[ "$extension_file" == 'Zip' ]]; then
		unzip -o "$path_file" -d "$_unpackDir" 1> /dev/null 2>&1
	elif [[ "$extension_file" == '7-zip' ]]; then
		cd "$_unpackDir" && 7z x "$path_file" 1> /dev/null 2>&1
	elif [[ "$extension_file" == 'Debian' ]]; then
		
		if [[ -f /etc/debian_version ]]; then    # Descompressão em sistemas DEBIAN
			ar -x "$path_file" 1> /dev/null 2>&1 
		else                                     # Descompressão em outros sistemas.
			ar -x "$path_file" --output="$_unpackDir" 1> /dev/null 2>&1
		fi
	fi

	# echo -e "$(date +%H:%M:%S)"
	if [[ $? == 0 ]]; then
		echo "OK"
		echo "0" > $SHELL_STATUS_FILE
	else
		print_erro "unpack_archive"
		echo "1" > $SHELL_STATUS_FILE
		return 1
	fi

	return 0
}


function exists_files()
{
    # arg 1 = strings/arquivos
    #
	# Verificar a existencia de arquivos, também suporta mais de um arquivo a ser testado.
    #
	# exists_files arquivo1 arquivo2 arquivo3 ...
	# se um arquivo informado como parâmetro não existir, esta função irá retornar 1.

	[[ -z $1 ]] && return 1
	export STATUS_OUTPUT=0

	while [[ $1 ]]; do
		if [[ ! -f "$1" ]]; then
			export STATUS_OUTPUT=1
			echo -e "ERRO ... o arquivo não existe $1"
            break
		fi
		shift
	done

	[[ "$STATUS_OUTPUT" == 0 ]] && return 0
	setStatusErro
	return 1
}



function addDesktopEntry() # -> None
{
	# arg 1 - $1 = Nome do arquivo a ser criado.
	# arg 2 - $2 = Um array com informações do arquivo.
    #    
    # Criar arquivo desktop - a extensão de arquivo (.desktop) é adicionada automáticamente.
	
	local _desktop_file="$1"; shift
	local _desktop_info="$@"; _desktop_file+=".desktop"
	local _path_file="$DIR_DESKTOP_ENTRY/$_desktop_file"

	echo -e "Criando arquivo .desktop -> $_desktop_file"
	echo '[Desktop Entry]' > "${_path_file}"
	
	while [[ "$1" ]]; do
		echo "Criando ... $1"
		echo -e "$1" >> "$_path_file"
		shift
	done

	chmod 777 "$_path_file"
}


function create_tempdir(){
	echo -e "$(mktemp --directory)"
}

function create_user_temp_dir(){
	local _d="${HOME}/TMP"
	mkdir -p "$_d"
	echo -e "$_d"
}

gpgVerify()
{
	# Verifica a integridade de um arquivo com gpg.
	# $1 = arquivo.asc
	# $2 = arquivo a ser verificado.

	echo -ne "Verificando integridade do arquivo ... $(basename $2) "
	gpg --verify "$1" "$2" 1> "$SHELL_STATUS_FILE" 2>&1
	if [[ $? == 0 ]]; then  
		echo "OK"
		return 0
	else
		printErro "GPG"
		sleep 1
		return 1
	fi
}



gpgImportKey()
{
	# Função para importar uma chave com o comando gpg --import <file>
	#
	#   gpgImportKey file
	
	if ! isFile "$1"; then
		printErro "gpgImportKey - nenhum arquivo encontrado"
		return 1
	fi

	printf "Importando key ... $1 "
	if gpg --import "$1" 1> /dev/null 2>&1; then
		echo "OK"
		return 0
	else
		printErro "gpgImportKey"
		return 1
	fi
	
}


function getSha256()
{
	# Retorna o sha256sum de um arquivo
	sha256sum "$1" | cut -d ' ' -f 1
}


function getSha512()
{
	# Retorna o sha512sum de um arquivo
	sha512sum "$1" | cut -d ' ' -f 1
}


function checkSha256()
{
    # arg 1 = arquivo
    # arg 2 = hash original
    #
	# Esta função compara a hash de um arquivo local no disco com
	# uma hash informada no parametro "$2" (hash original). 
	#   Ou seja "$1" é o arquivo local e "$2" é uma hash
	# if ! isFile $1; then print_erro_param "checkSha256"; return 1; fi
	
	# Verificar os parâmetros.
	if [[ -z "$2" ]] || [[ ! -f $1 ]]; then
		print_erro_param "checkSha256"
		return 1
	fi

	# Verificar se o tamanho do hash a verifcar contém os 64 caracteres.
	if [[ "${#2}" != 64 ]]; then

		return 1
	fi

	echo -ne "Verificando integridade do arquivo ... $(basename $1) "
	local _hash_file=$(getSha256 $1)

	# Calucular o tamanho do arquivo
	# len_file=$(du -hs $1 | awk '{print $1}')
	# printf "%-15s%65s\n" "HASH original" "$2"
	# printf "%-15s%65s\n" "HASH local" "$hash_file"

	if [[ "$_hash_file" == "$2" ]]; then echo 'OK'; return 0; fi
	
	red "ERRO"
	red "arquivo inseguro ... $1"
	return 1
}




function checkSha512()
{
    # arg 1 = arquivo/string
    # arg 2 = hash original
    #
	# Esta função compara a hash de um arquivo local no disco com
	# uma hash informada no parametro "$2" (hash original). 
	#   Ou seja "$1" é o arquivo local e "$2" é uma hash
	# if ! isFile $1; then print_erro_param "checkSha256"; return 1; fi
	
	if [[ -z "$2" ]] || [[ ! -f $1 ]]; then
		print_erro_param "checkSha512"
		return 1
	fi

	echo -ne "Verificando integridade do arquivo ... $(basename $1) "
	local _hash_file=$(getSha512 $1)

	# Calucular o tamanho do arquivo
	# len_file=$(du -hs $1 | awk '{print $1}')
	# printf "%-15s%65s\n" "HASH original" "$2"
	# printf "%-15s%65s\n" "HASH local" "$hash_file"

	if [[ "$_hash_file" == "$2" ]]; then echo 'OK'; return 0; fi
	
	red "ERRO"
	red "arquivo inseguro ... $1"
	return 1
}
