#!/usr/bin/env bash
#
#

function define_dirs_user_root(){
	export DIR_BIN='/usr/local/bin'
	export DIR_LIB='/usr/local/lib'
	export DIR_OPTIONAL='/opt'
	export DIR_OPT="${DIR_OPTIONAL}"
	export DIR_SHARE='/usr/share'
	export DIR_THEMES='/usr/share/themes'
	export DIR_DESKTOP_ENTRY='/usr/share/applications'
	export DIR_ICONS='/usr/share/icons/hicolor/256x256/apps'
	export DIR_HICOLOR='/usr/share/icons/hicolor'
	export DIR_UNPACK='/tmp/shell-libs/unpack'
	export DIR_DOWNLOADS_CACHE='/tmp/downloads'
}

function define_dirs_user(){
	[[ $(id --user) == 0 ]] && return 0
	export DIR_BIN=~/bin
	export DIR_HOME_BIN=~/bin
	export DIR_LIB=~/.local/lib
	export DIR_SHARE=~/.local/share
	export DIR_THEMES=~/.themes
	export DIR_OPTIONAL=~/opt
	export DIR_OPT="${DIR_OPTIONAL}"
	export DIR_DESKTOP_ENTRY=~/.local/share/applications
	export DIR_ICONS=~/.local/share/icons
	export DIR_HICOLOR=~/.local/share/icons/hicolor
	export DIR_UNPACK=~/TMP/unpack
	export DIR_DOWNLOADS_CACHE="${HOME}/Downloads/appcli-downloads"
}

function createUserDirs(){
	[[ $(id --user) == 0 ]] && return 0

	# Criar os diretório na HOME do usuário.
	mkdir -p "$DIR_BIN"
	mkdir -p "$DIR_HOME_BIN"
	mkdir -p "$DIR_LIB"
	mkdir -p "$DIR_SHARE"
	mkdir -p "$DIR_THEMES"
	mkdir -p "$DIR_OPTIONAL"
	mkdir -p "$DIR_DESKTOP_ENTRY"
	mkdir -p "$DIR_ICONS"
	mkdir -p "$DIR_HICOLOR"	
	mkdir -p "$DIR_UNPACK"
	mkdir -p "$DIR_DOWNLOADS_CACHE"
}

function setDirsRoot(){
    define_dirs_user_root
}

function setDirsUser(){
    if [[ $(id -u) == 0 ]]; then
        printErro "setDirsUser: você não poder ser o root para executar essa ação."
        return
    fi

    define_dirs_user
	createUserDirs
}

if [[ $(id --user) == 0 ]]; then
	define_dirs_user_root
else
	define_dirs_user
fi

