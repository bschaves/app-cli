#!/usr/bin/env bash
#
# 
# Esse arquivo inicializa variáveis, e o core de funções shell-libs
# 


# SHELL_LIBS -> deve apontar para esse diretório 
if [[ -z $SHELL_LIBS ]]; then
	echo -e "Erro -> o diretório SHELL_LIBS não foi definido"
	exit 1
fi

SHELL_DEVICE_FILE="/tmp/$(whoami)-device.txt"
SHELL_STATUS_FILE="/tmp/$(whoami)-status.txt"
export STATUS_OUTPUT=0

#mkdir -p /tmp/$(whoami)
touch "$SHELL_DEVICE_FILE"
touch "$SHELL_STATUS_FILE"

source ~/.bashrc

[[ -z $HOME ]] && HOME=~/

# Inserir ~/.local/bin em PATH se não existir.
echo "$PATH" | grep -q "$HOME/.local/bin" || {
	export PATH="$HOME/.local/bin:$PATH"
}

function show_not_found_module(){
	echo -e "Falha ao tentar importar -> $@"
}


export readonly LIB_COMMON="${SHELL_LIBS}/common/common.sh"
export readonly LIB_VERSION="${SHELL_LIBS}/common/version.sh"
export readonly LIB_SYSTEM="${SHELL_LIBS}/system/sys.sh"
export readonly LIB_APP_DIRS="${SHELL_LIBS}/system/appdirs.sh"
export readonly LIB_REQUESTS="${SHELL_LIBS}/request/requests.sh"
export readonly LIB_APT_BASH="${SHELL_LIBS}/system/apt-bash.sh"

# Importar módulos common
source "${LIB_COMMON}" 1> /dev/null 2>&1 || show_not_found_module "${LIB_COMMON}"
source "${LIB_VERSION}" 1> /dev/null 2>&1 || show_not_found_module "${LIB_VERSION}"

# Importar módulos system
source "${LIB_SYSTEM}" 1> /dev/null 2>&1 || show_not_found_module "${LIB_SYSTEM}"
source "${LIB_APP_DIRS}" 1> /dev/null 2>&1 || show_not_found_module "${LIB_APP_DIRS}"
source "${LIB_APT_BASH}" 1> /dev/null 2>&1 || show_not_found_module "${LIB_APT_BASH}"

# Importar módulos request
source "${LIB_REQUESTS}" 1> /dev/null 2>&1 || show_not_found_module "${LIB_REQUESTS}"







