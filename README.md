# app-cli

Serve para instalar alguns aplicativos em sistemas Linux, por meio da linha de comando.


# REQUISITOS:

python3 e os pacotes python3-pip, python3-setuptools ou equivalentes.

sudo apt install python3 python3-pip python3-setuptools

# DOWNLOAD E CONFIGURAÇÃO.

Baixe ou clone o projeto, em seguida entre na pasta principal e execute:

   $ python3 configure.py - (Necessário ter conexão com a internet)

O comando acima irá instalar as demais dependências em seu sistema, são elas:

apps_conf: https://gitlab.com/bschaves/apps-conf

cmdlib: https://gitlab.com/bschaves/cmd-lib

python-magic: https://github.com/ahupp/python-magic

tqdm

requests

bs4

