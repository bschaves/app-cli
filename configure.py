#!/usr/bin/env python3

"""
Script para configurar o appcli no sistema.
Versão: 2024-08-04

"""

import os
import sys
import shutil
from pathlib import Path


_this_file = os.path.realpath(__file__)
_this_dir = os.path.dirname(_this_file)


def print_line():
    print('------------------------------------------------')

def mkdir(path_dir:str):
    if Path(path_dir).exists():
        return True
    os.makedirs(path_dir)
    return True

def get_user_home():
    return Path().home()

class LocalDirs(object):
    def __init__(self, user_home: str, *, root:bool=True) -> None:
        self.user_home = user_home
        self.root = root
        self.script_name = 'appcli-local'

    def dirBin(self) -> str:
        return os.path.join(self.user_home, 'bin')
    
    def dirOpt(self) -> str:
        return os.path.join(self.user_home, 'opt')
    
    def file_script(self) -> str:
        return os.path.join(self.dirBin(), self.script_name)
    
    def dirInstall(self) -> str:
        return os.path.join(self.dirOpt(), 'appcli-local')

    def createDirs(self):
        mkdir(self.dirBin())
        mkdir(self.dirOpt())
        mkdir(self.dirInstall())

class LocalDirsLinux(LocalDirs):
    def __init__(self, user_home: str, *, root: bool = True) -> None:
        super().__init__(user_home, root=root)
        self.root = root
        self.user_home = user_home

    def dirBin(self) -> str:
        if self.root is True:
            return '/usr/local/bin'
        return super().dirBin()
    
    def dirOpt(self) -> str:
        if self.root is True:
            return '/opt'
        return super().dirOpt()
    
    def file_script(self) -> str:
        if self.root is True:
            return os.path.join(self.dirBin(), self.script_name)
        return super().file_script()
    
    def dirInstall(self) -> str:
        return os.path.join(self.dirOpt(), 'appcli-local')

    def createDirs(self):
        if self.root is True:
            print(f'Criando diretório {self.dirBin()}')
            os.system(f'sudo mkdir -p {self.dirBin()}')
            print(f'Criando diretório {self.dirOpt()}')
            os.system(f'sudo mkdir -p {self.dirOpt()}')
            print(f'Criando diretório {self.dirOpt()}')
            os.system(f'sudo mkdir -p {self.dirInstall()}')

            return
        return super().createDirs()
    
def configure_script(local_dirs: LocalDirs):

    file_executable = os.path.join(local_dirs.dirInstall(), 'main.sh')
    print(f'Configurando script: {local_dirs.file_script()}')
    if local_dirs.root is True:
        os.system(f'sudo ln -sf {file_executable} {local_dirs.file_script()}')
        os.system(f'sudo chmod +x {local_dirs.file_script()}')
    else:
        os.system(f'ln -sf {file_executable} {local_dirs.file_script()}')
        os.system(f'chmod +x {local_dirs.file_script()}')

def install_files(local_dirs: LocalDirs):
    
    local_dirs.createDirs()
    print(f'Copiando arquivos para {local_dirs.dirInstall()}')
    os.chdir(_this_dir)
    if local_dirs.root is True:
        os.system(f'sudo cp -R * {local_dirs.dirInstall()}/')
    else:
        os.system(f'cp -R * {local_dirs.dirInstall()}/')

    configure_script(local_dirs)
        
def uninstall_files(local_dirs: LocalDirs):
    print(f'Apagando: {local_dirs.dirInstall()}')
    if local_dirs.root is True:
        os.system(f'sudo rm -rf {local_dirs.dirInstall()}')
        os.sysconf(f'sudo rm {local_dirs.file_script()}')
    else:
        os.system(f'rm -rf {local_dirs.dirInstall()}')
        os.sysconf(f'rm {local_dirs.file_script()}')
    

def main():
    local_dirs = LocalDirsLinux(get_user_home(), root=True)
    install_files(local_dirs)


if __name__ == '__main__':
    main()
