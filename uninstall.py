#!/usr/bin/env python3

"""
Script para configurar o appcli no sistema.
Versão: 2024-08-04

"""

import os
import sys
from pathlib import Path

_this_file = os.path.realpath(__file__)
_this_dir = os.path.dirname(_this_file)

def print_line():
    print('------------------------------------------------')

def get_user_home():
    return Path().home()

class LocalDirs(object):
    def __init__(self, user_home: str, *, root:bool=True) -> None:
        self.user_home = user_home
        self.root = root
        self.script_name = 'appcli-local'

    def dirBin(self) -> str:
        return os.path.join(self.user_home, 'bin')
    
    def dirOpt(self) -> str:
        return os.path.join(self.user_home, 'opt')
    
    def file_script(self) -> str:
        return os.path.join(self.dirBin(), self.script_name)
    
    def dirInstall(self) -> str:
        return os.path.join(self.dirOpt(), 'appcli-local')

    def createDirs(self):
        mkdir(self.dirBin())
        mkdir(self.dirOpt())
        mkdir(self.dirInstall())

class LocalDirsLinux(LocalDirs):
    def __init__(self, user_home: str, *, root: bool = True) -> None:
        super().__init__(user_home, root=root)
        self.root = root
        self.user_home = user_home

    def dirBin(self) -> str:
        if self.root is True:
            return '/usr/local/bin'
        return super().dirBin()
    
    def dirOpt(self) -> str:
        if self.root is True:
            return '/opt'
        return super().dirOpt()
    
    def file_script(self) -> str:
        if self.root is True:
            return os.path.join(self.dirBin(), self.script_name)
        return super().file_script()
    
    def dirInstall(self) -> str:
        return os.path.join(self.dirOpt(), 'appcli-local')

    def createDirs(self):
        if self.root is True:
            print(f'Criando diretório {self.dirBin()}')
            os.system(f'sudo mkdir -p {self.dirBin()}')
            print(f'Criando diretório {self.dirOpt()}')
            os.system(f'sudo mkdir -p {self.dirOpt()}')
            print(f'Criando diretório {self.dirOpt()}')
            os.system(f'sudo mkdir -p {self.dirInstall()}')

            return
        return super().createDirs()
        
def uninstall_files(local_dirs: LocalDirs):
    print(f'Apagando:\n {local_dirs.dirInstall()}')
    print(f' {local_dirs.file_script()}')
    
    if local_dirs.root is True:
        os.system(f'sudo rm -rf {local_dirs.dirInstall()}')
        os.system(f'sudo rm {local_dirs.file_script()}')
    else:
        os.system(f'rm -rf {local_dirs.dirInstall()}')
        os.system(f'rm {local_dirs.file_script()}')
    

def main():
    local_dirs = LocalDirsLinux(get_user_home(), root=True)
    uninstall_files(local_dirs)


if __name__ == '__main__':
    main()
