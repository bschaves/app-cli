#!/usr/bin/env python3

__version__ = '0.3.7' # 2024-04-05
__author__ = 'Bruno Chaves'
__repo__ = 'https://gitlab.com/bschaves/app-cli'
__download_file__ = 'https://gitlab.com/bschaves/app-cli/-/archive/main/app-cli-main.zip'
__download_dev_file__='https://gitlab.com/bschaves/app-cli/-/archive/dev/app-cli-dev.zip'