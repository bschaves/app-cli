#!/usr/bin/env python3

import os
import sys
import re
import shutil
from getpass import getuser

from appcli.common.apps_conf import (
    HOME,
    KERNEL_TYPE,
    mkdir,
    FilePath,
    FileReader,
    AppLocalDirs,
    FileJson,
    SystemLocalDirs,
    add_home_in_path,
    touch,
    get_abspath,
    unpack,
    shasum,
    shellcore,
    sudo_command,
    device_is_mounted,
    get_term_colums,
    is_admin,
    ExecShellCommand,
    gpg_utils,
)


# Módulos locais
from appcli.common.apps_conf import (DownloaderRequest, is_executable, get_mount_dirs, HOME)
from appcli.common.utils import (
                            LinuxInfo,
                            json,
                            print_line,
                            Colors
                            )



if KERNEL_TYPE == 'Linux':
    linux_info: LinuxInfo = LinuxInfo()

Color = Colors()


# Objeto para ler e manupular as configurações, que são padrão caso não tenha o arquivo de configuração.
# caminho padrão do arquivo de configuração ~/appcli.json

class SettingsAndDirsThisApp(AppLocalDirs):
    def __init__(self, *, type_root: bool = False, appname:str = None, file_config_json:FileJson=None) -> None:
        super().__init__(type_root=type_root, appname=appname)
        self.file_config_json: FileJson = file_config_json  
        self.config_in_local_file: dict = None 
        if self.file_config_json.file_path_json.exists():
            try:
                self.config_in_local_file = self.file_config_json.lines_to_dict()
            except Exception as e:
                print(f'{__class__.__name__} {e}')

    def get_value_config(self, key_value: str) -> str:
        """
            Recebe um valor/key se a chave existir no conteúdo de configurações, retornar o valor correspondente
        se não, retorna None
        """
        if self.config_in_local_file is None:
            return None
        if not key_value in self.config_in_local_file:
            return None
        
        return self.config_in_local_file.get(key_value)
        
    def dircache(self) -> str:
        
        if self.get_value_config('DIR_CACHE') is None:
            return super().dircache()
        return self.get_value_config('DIR_CACHE')
        
    
class BuildSettingsAndDirsThisApp(object):
    def __init__(self) -> None:
        self._appname = None
        self._type_root = False
        self._file_config_json = None

    def set_appname(self, new_appname):
        self._appname = new_appname
        return self
    
    def set_type_root(self, new_type_root):
        self._type_root = new_type_root
        return self
    
    def set_file_config_json(self, new_file_json):
        self._file_config_json = new_file_json
        return self
    
    def build(self) -> SettingsAndDirsThisApp:
        if self._appname is None:
            print(f'{__class__.__name__} self._appname não foi definido')
            sys.exit(1)
        elif self._file_config_json is None:
            print(f'{__class__.__name__} self._file_config_json não foi definido')
            sys.exit(1)

        return SettingsAndDirsThisApp(type_root=self._type_root, appname=self._appname, file_config_json=self._file_config_json)


# Objeto para manipular o arquivo de configuração.
file_config_this_app:str = os.path.join(HOME, 'appcli.json')
file_config_json_this_app: FileJson = FileJson(FilePath(file_config_this_app))
this_appname = 'appcli'    

download_manager: DownloaderRequest = DownloaderRequest()
appcli_conf_dirs = BuildSettingsAndDirsThisApp().set_appname(this_appname).set_file_config_json(file_config_json_this_app).build()
appcli_conf_dirs.create_dirs()

class Package(AppLocalDirs):
    def __init__(self, *, type_root:bool = False, appname:str = None, url:str = None, pkg_file_name:str = None):
        super().__init__(type_root=type_root, appname=appname)
        self.url: str = None
        self.pkg_file_name: str = None # Nome do arquivo/pacote de instalação.
        self.pkg_class = None
        self.pkg_hash_value = None
        self.assume_yes: bool = False
        self.bool_stdout: bool = False
        self.text_stdout: str = None
        self.dir_project: str = None

    @property
    def dir_project(self) -> str:
        return self._dir_project

    @dir_project.setter
    def dir_project(self, new_dir: str):
        if new_dir is None:
            self._dir_project = None
            return
        if os.path.isdir(new_dir):
            self._dir_project = new_dir

    @property
    def pkg_file_name(self):
        return  self._pkg_file_name

    @pkg_file_name.setter
    def pkg_file_name(self, new_file_name: str):
        self._pkg_file_name = new_file_name

    @property
    def url(self) -> str:
        return self._url

    @url.setter
    def url(self, new_url):
        self._url = new_url

    def chdir(self):
        if self.dir_project is None:
            print(f'{__class__.__name__} ERRO -> dir_project é Nulo')
            return
        
        os.chdir(self.dir_project)
        os.chdir('bin')
        os.chmod('install-android-studio', 0o777)

    def pkg_file_path(self) -> FilePath:
        return FilePath(self.pkg_file_abspath())

    def pkg_file_abspath(self) -> str:
        """
            Retorna o caminho absoluto do pacote de instalação. O nome do pacote/arquivo é concatenado
        com o diretório de cache para downloads.

            O caminho padrão é fornecido pela classe AppcliLocalDirs, mas pode ser alterado manualmente no arquivo 
        de configuração ~/appcli.json
        
        """
        if self.pkg_file_name is None:
            print(f'{__class__.__name__} ERRO ... o nome do pacote está vazio.')
            return None
        return os.path.join(appcli_conf_dirs.dircache(), self.pkg_file_name)

    def download(self) -> bool:
        """
           Realiza o download dos arquivos necessários para instalação do pacote. Em alguns pacotes
        Pode ser necessário baixar uma chave de assinatura (.asc) ou importar uma chave pública para
        verificação, estas etapas devem ser feita neste método para uma possível verificação offline.
           Se for necessário baixar mais alguma informação além do self.pkg_file_path(), esse método
        dever ser reescrito na classe FILHA.
        """
        if self.url is None or self.pkg_file_abspath() is None:
            return False
        return download_manager.download_file(self.url, self.pkg_file_abspath())

    def create_desktop_entry(self, desktop_info: list) -> None:
        """
            Recebe uma lista com os dados do arquivo .desktop, e escreve em um arquivo
        temporário, em seguida copia o arquivo temporário para self.desktop_entry(file_name)
        """

        if self.pkg_file_abspath() is None:
            return

        if not isinstance(desktop_info, list):
            Exception(f'{__class__.__name__} ERRO ... tipo de dados incorreto {desktop_info}')
            sys.exit(1)

        print(f'Criando arquivo {self.desktop_entry(self.appname)}')
        tmp_file = FilePath(self.get_temp_file())
        file_reader: FileReader = FileReader(tmp_file)
        file_reader.write_lines(['[Desktop Entry]'])
        file_reader.append_lines(desktop_info)
        os.chmod(tmp_file.absolute(), 0o755)
        shellcore.copy(tmp_file.absolute(),  self.desktop_entry(self.appname))
        shellcore.rmdir(tmp_file.absolute())

    def verify(self) -> bool:
        """Verifica a integridade do arquivo/pacote de instalação"""
        pass

    def install(self) -> bool:
        pass

    def uninstall(self) -> bool:
        if self.get_installed_files() == []:
            return

        for f in self.get_installed_files():
            shellcore.rmdir(f)

    def clean_cache(self) -> None:
        pass

    def version(self) -> str:
        pass

    def create_script(self):
        pass

    def is_installed(self) -> bool:
        """Verifica se um pacote está ou não instalado"""
        pass

    def get_info(self) -> dict:
        """Retorna informações sobre o pacote."""
        _info = {}
        if self.pkg_file_name is not None:
            _info.update({"instalador": self.pkg_file_abspath()})
        if self.url is not None:
            _info.update({"url": self.url})

        return _info

    def get_installed_files(self) -> list:
        """Retorna os arquivos instalados no sistema"""
        return []

    def get_info_json(self):
        """Retorna informações sobre um pacote, no formato json."""
        return json.dumps(self.get_info(), ensure_ascii=False, indent=4, sort_keys=True)


class PackageDebian(Package):
    def __init__(self, *, type_root: bool = False, appname: str):
        super().__init__(type_root=type_root, appname=appname)
        self._apt_pkg_name: str = None

    def check_deb(self) -> bool:
        """Verificar se todos as dependências estão instaladas."""
        pass

    def install_deps(self):
        """Instalar dependências do pacote Debian"""
        pass

    def install(self) -> bool:

        if FilePath(self.pkg_file_abspath()).extension() != '.deb':
            print(f'{__class__.__name__} ERRO {self.pkg_file_abspath()} não é um arquivo Debian.')
            return False

        self.install_deps()
        print_line('-')
        print(f'Executando ... sudo apt install {self.pkg_file_abspath()}')
        os.system(f'sudo apt install {self.pkg_file_abspath()}')

    def uninstall(self) -> bool:
        if self._apt_pkg_name is not None:
            os.system(f'sudo apt remove {self._apt_pkg_name}')

#=====================================================================#


class ConfigureAppImage(Package):
    def __init__(self, *, type_root: bool = False, appname: str, url: str = None, pkg_file_name: str = None):
        super().__init__(type_root=type_root, appname=appname)


#=====================================================================#
 
class AndroidStudioTarGz(Package):
    def __init__(self, *, type_root: bool = False, appname='android-sdk'):
        super().__init__(type_root=type_root, appname=appname)

    def chdir(self):
        os.chdir(self.dir_project)
        os.chdir('bin')
        os.chmod('install-android-studio', 0o777)
    def install(self) -> bool:
        self.chdir()
        os.system('./install-android-studio -i')
    def uninstall(self) -> bool:
        self.chdir()
        os.system('./install-android-studio -u')

    def download(self) -> bool:
        self.chdir()
        os.system('./install-android-studio -d')

class AtomDebFile(PackageDebian):

    # https://github.com/atom/atom/releases
    # https://atom.io

    def __init__(self, *, type_root: bool = False, appname: str):
        super().__init__(type_root=type_root, appname=appname)
        self.pkg_file_name = 'atom-amd64.deb'
        self.pkg_class = 'Desenvolvimento'
        #self.url = 'https://atom-installer.github.com/v1.58.0/atom-amd64.deb?s=1627025597&ext=.deb'
        self.url = 'https://github.com/atom/atom/releases/download/v1.58.0/atom-amd64.deb'

    def verify(self) -> bool:
        return True


class AtomTarGz(Package):

    # SHA256:
    #   c0039a6770bd84de957a0fb98def9db7026b26bfe98dde752f874e28387f7cc1

    def __init__(self, *, type_root: bool = False, appname: str):
        super().__init__(type_root=type_root, appname=appname)

        self.url = 'https://github.com/atom/atom/releases/download/v1.58.0/atom-amd64.tar.gz'
        self.pkg_file_name = 'atom-amd64.tar.gz'
        self.pkg_class = 'Desenvolvimento'

    def verify(self) -> bool:
        return shasum.check_sha256(
            self.pkg_file_abspath(),
            'c0039a6770bd84de957a0fb98def9db7026b26bfe98dde752f874e28387f7cc1'
        )

    def version(self) -> str:
        return '1.58'

    def create_script(self):
        f_reader = FileReader(FilePath(self.script()))
        lines = [
            "#!/usr/bin/env bash",
            " ",
            "work_dir=$(pwd)",
            f"cd {self.appdir()}",
            "./atom $@",
            "cd $work_dir"
        ]
        f_reader.write_lines(lines)
        os.chmod(self.script(), 0o777)


    def install(self) -> bool:
        if os.path.isdir(self.appdir()):
            shellcore.rmdir(self.appdir())
        
        unpack(self.pkg_file_abspath(), output_dir=self.get_temp_dir(create=True))
        print('Copiando arquivos')
        sys.stdout.flush()
        os.chdir(self.get_temp_dir())
        shellcore.copy('./atom-1.58.0-amd64', self.appdir())
        os.chdir(self.appdir())
        shellcore.copy('./atom.png', self.icon('atom.png'))
        shellcore.rmdir(self.get_temp_dir())
        self.create_script()

        info = [
            "Name=Atom",
            "Comment=Code Editing.",
            "GenericName=Text Editor",
            f"Exec={self.script()}",
            f"Icon={self.icon('atom.png')}",
            "Type=Application",
            "StartupNotify=false",
            "Categories=Utility;TextEditor;Development;IDE;",
            "Keywords=atom;",
        ]

        self.create_desktop_entry(info)

    def get_installed_files(self) -> list:
        return [
            self.appdir(),
            self.desktop_entry('atom'),
            self.icon('atom.png'),
            self.script(),
        ]

    def uninstall(self) -> bool:
        for f in self.get_installed_files():
            shellcore.rmdir(f)

class BalenaEtcherAppimage(Package):
    """
    Instalação do Etcher no formato AppImage em sistemas Linux

    1 - Download do arquivo AppImage.
    2 - Verificar sha512sum
    3 - Copiar os arquivos para os diretórios de instalação.
    """
    def __init__(self, *, type_root=False, appname):
        super().__init__(type_root=type_root, appname=appname)
        # https://github.com/balena-io/etcher/releases

        self.pkg_icon_hash = '8abfa3077b1611d20f7994a4ff518417ceb80794e32db35e4c9227f3c5d6956dcc3bfd1d8d4b0da201d64b731ce3ac3d7d97537741e0fe98692abdbf098e35e1'
        #self.url = 'https://github.com/balena-io/etcher/releases/download/v1.6.0/balenaEtcher-1.6.0-x64.AppImage'
        self.url = 'https://github.com/balena-io/etcher/releases/download/v1.19.21/balenaEtcher-1.19.21-x64.AppImage'
        self.pkg_hash = ''
        self.pkg_url_icon = 'https://raw.github.com/balena-io/etcher/master/assets/icon.png'
        self.pkg_file_name = 'balenaEtcher-1.19.21-x64.AppImage'
        self.pkg_class = 'Acessórios'
        self.pkg_hash_value = None

    def download(self) -> bool:
        download_manager.download_file(self.pkg_url_icon, self.get_icon_cache())
        return super().download()

    def version(self) -> str:
        return '1.6.0'

    def get_icon_cache(self) -> str:
        """Retorna o arquivo icone no em cache"""
        return os.path.join(appcli_conf_dirs.dircache(), 'balena-etcher.png')

    def getAppImagePath(self) -> str:
        """Caminho absoluto do AppImage em ~/.local/opt ou /opt"""
        return os.path.join(self.appdir(), 'balenaEtcher.AppImage')

    def install(self) -> bool:
        if os.path.isdir(self.appdir()):
            shellcore.rmdir(self.appdir())
       
        shellcore.mkdir(self.appdir())
        shellcore.copy(self.get_icon_cache(), self.icon('balena-etcher.png'))
        shellcore.copy(self.pkg_file_abspath(), self.getAppImagePath())
        os.chmod(self.getAppImagePath(), 0o777)
        self.create_script()

        desktop_info = [
            'Name=balenaEtcher',
            f'Exec={self.getAppImagePath()} --no-sandbox',
            'Terminal=false',
            'Type=Application',
            f'Icon={self.icon("balena-etcher.png")}',
            'StartupWMClass=balenaEtcher',
            'Comment=Flash OS images to SD cards and USB drives, safely and easily.',
            'MimeType=x-scheme-handler/etcher',
            'Categories=Utility;',
        ]

        self.create_desktop_entry(desktop_info)


    def create_script(self):
        f_reader = FileReader(FilePath(self.get_temp_file()))
        lines = [
            '#!/usr/bin/env bash',
            f'{self.getAppImagePath()} --no-sandbox'
        ]

        f_reader.write_lines(lines)
        os.chmod(self.get_temp_file(), 0o777)
        shellcore.copy(self.get_temp_file(), self.script())
        shellcore.rmdir(self.get_temp_file())

    def verify(self) -> bool:
        if not shasum.check_sha512(self.get_icon_cache(), self.pkg_icon_hash):
            return False
        return shasum.check_sha512(self.pkg_file_abspath(), self.pkg_hash)

    def get_installed_files(self) -> list:
        return [
            self.appdir(),
            self.desktop_entry(self.appname),
            self.icon('balena-etcher.png'),
            self.script(),
        ]

class GoogleChrome(PackageDebian):
    def __init__(self, *, type_root: bool = False, appname: str):
        super().__init__(type_root=type_root, appname=appname)
        self.url = 'https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb'
        self.pkg_file_name = 'google-chrome-stable_current_amd64.deb'
        
    def verify(self) -> bool:
        return True
    

class ElectrumAppImage(Package):

    # GITHUB:
    #   https://github.com/spesmilo/electrum
    #
    # DOWNLOADS:
    #   https://electrum.org/#download
    #
    # PUB KEY: 
    #   https://raw.githubusercontent.com/spesmilo/electrum/master/pubkeys/ThomasV.asc
    #
    #

    def __init__(self, *, type_root: bool = False, appname: str):
        super().__init__(type_root=type_root, appname=appname)
        self.url = 'https://download.electrum.org/4.1.5/electrum-4.1.5-x86_64.AppImage'
        self.pkg_file_name = 'electrum-4.1.5-x86_64.AppImage'
        self.pkg_hash_value = '0b93801e52706091b5be0219a8f7fb6f04a095f7c5dd8bf9a0a93f5f5d6ed98e'

    def get_sing_file(self) -> str:
        return os.path.join(appcli_conf_dirs.dircache(), 'electrum-4.1.5-x86_64.AppImage.asc')

    def file_icon_cache(self) -> str:
        return os.path.join(appcli_conf_dirs.dircache(), 'electrum.png')

    def download(self) -> bool:
        # https://raw.githubusercontent.com/spesmilo/electrum/master/electrum/gui/icons/electrum.png
        download_manager.download_file(
            'https://raw.githubusercontent.com/spesmilo/electrum/master/electrum/gui/icons/electrum.png',
            self.file_icon_cache()
        )
        return super().download()

    def verify(self) -> bool:
        if not shasum.check_sha256(
            self.file_icon_cache(),
            '0b93801e52706091b5be0219a8f7fb6f04a095f7c5dd8bf9a0a93f5f5d6ed98e'
        ):
            return False

        return shasum.check_sha256(
            self.pkg_file_abspath(),
            '21d5017ddf87d75be76a3c736fb547cb5e33399938abd69d82ff66a80de8c13f'
        )
        
    def install(self) -> bool:
        if os.path.isdir(self.appdir()):
            shellcore.rmdir(self.appdir())
        shellcore.mkdir(self.appdir())

        shellcore.copy(self.pkg_file_abspath(), f'{self.appdir()}/electrum.AppImage')
        shellcore.copy(self.file_icon_cache(), self.icon('electrum.png'))
        os.chmod(f'{self.appdir()}/electrum.AppImage', 0o777)

        desktop_info = [
            "Encoding=UTF-8",
            "Name=Electrum Bitcoin Wallet",
            "Comment=Lightweight Bitcoin Client",
            "GenericName=Bitcoin Wallet",
            f"Exec={self.script()}",
            f"Icon={self.icon('electrum.png')}",
            "Categories=Finance;Network;",
            "StartupNotify=true",
            f"Version={self.version()}",
            "Type=Application",
            "Terminal=false",
            "MimeType=x-scheme-handler/bitcoin;x-scheme-handler/lightning;",
        ]

        self.create_desktop_entry(desktop_info)
        os.system('gtk-update-icon-cache')
        self.create_script()
        shellcore.rmdir(self.get_temp_dir())

    def create_script(self):
        file_reader = FileReader(FilePath(self.script()))
        lines = [f'{self.appdir()}/electrum.AppImage $@']
        file_reader.write_lines(lines)
        os.chmod(self.script(), 0o777)

    def get_installed_files(self) -> list:
        return [
            self.appdir(),
            self.desktop_entry(self.appname),
            self.icon('electrum.png'),
        ]


class Flutter(Package):
    def __init__(self, *, type_root: bool = False, appname: str = None, url: str = None, pkg_file_name: str = None):
        super().__init__(type_root=type_root, appname=appname, url=url, pkg_file_name=pkg_file_name)
        # https://terminalroot.com.br/2023/01/como-instalar-flutter-e-dart-em-qualquer-distro-gnu-linux.html
        # https://dev.to/fullstackhacker/install-flutter-on-debian-3g3h
        self.url = 'https://storage.googleapis.com/flutter_infra_release/releases/stable/linux/flutter_linux_3.19.3-stable.tar.xz'
        self.pkg_file_name = 'flutter_linux_3.19.3-stable.tar.xz'
        self.pkg_hash_value = 'ddb712be586281afd51a3f96db3681f99f242278201e0ae3322c03706e4004ae'

    def script(self) -> str:
        return super().script()
    
    def install_requirements(self):
        
        os.system(f'sudo apt install curl git unzip xz-utils zip libglu1-mesa')
        os.system(f'sudo apt install lib32stdc++6')

    def install(self) -> bool:
        
        if os.path.isdir(self.appdir()):
            shellcore.rmdir(self.appdir())
        
        unpack(self.pkg_file_abspath(), output_dir=self.get_temp_dir(create=True), verbose=True)
        os.chdir(self.get_temp_dir())
        
        for _dir in os.listdir('.'):
            if ('flutter' in _dir) and (os.path.isdir(_dir)):
                shellcore.copy(_dir, self.appdir())
                break
        os.chdir(os.path.join(self.appdir(), 'bin'))
        os.chmod('flutter', 0o777)
        os.symlink(os.path.join(self.appdir(), 'bin', 'flutter'), self.script())
        shellcore.rmdir(self.get_temp_dir())

        self.install_requirements()
        print_line()
        print(f'{Color.YELLOW} Reinicie o computador {Color.RESET}')

    def verify(self) -> bool:
       return shasum.check_sha256(self.pkg_file_abspath(), self.sha256sum)
    
    def get_installed_files(self) -> list:
        return [
            self.appdir(),
            self.script(),
        ]


class Genymotion(Package):
    def __init__(self, *, type_root: bool = False, appname: str):
        super().__init__(type_root=type_root, appname=appname)
        self.url = 'https://dl.genymotion.com/releases/genymotion-3.1.2/genymotion-3.1.2-linux_x64.bin'
        self.pkg_file_name = 'genymotion-3.1.2-linux_x64.bin'

    def verify(self) -> bool:
        return shasum.check_sha256(
            self.pkg_file_abspath(),
            '41e9b4486a73468ecefbf7aa9a63292281c69cb45fe0cb193d290b2b921d0433',
        )

    def version(self) -> str:
        return '3.1.2'

    def get_file_bin(self) -> str:
        return f'{self.appdir()}/genymotion-x64.bin'

    def install(self) -> bool:
        if os.path.isdir(self.appdir()):
            shellcore.rmdir(self.appdir())

        shellcore.mkdir(self.appdir())
        shellcore.copy(self.pkg_file_abspath(), self.get_file_bin())
        self.create_script()

    def create_script(self):
        lines = [
            '#!/usr/bin/env bash',
            ' ',
            f'cd {self.appdir()}',
            f'{self.get_file_bin()} $@',
        ]

        f_reader = FileReader(FilePath(self.script()))
        f_reader.write_lines(lines)
        os.chmod(self.get_file_bin(), 0o777)
        os.chmod(self.script(), 0o777)

    def get_installed_files(self) -> list:
        return [
            self.appdir(),
            self.script(),
        ]



class KdenLiveAppImage(Package):
    def __init__(self, *, type_root: bool = False, appname: str):
        super().__init__(type_root=type_root, appname=appname)
        self.url = 'https://download.kde.org/stable/kdenlive/21.12/linux/kdenlive-21.12.2-x86_64.appimage'
        self.pkg_file_name = 'kdenlive-21.12.2-x86_64.appimage'

    def path_bin(self) -> str:
        """
           Retorna o caminho absoluto do destino do binário/appimage instalado. 
        EX: 
           ~/.local/opt/kdenlive/kdenlive.appimage    
        """
        return os.path.join(self.appdir(), 'kdenlive.AppImage')

    def version(self) -> str:
        return '21.12.2'

    def install(self) -> bool:
        if os.path.isdir(self.appdir()):
            shellcore.rmdir(self.appdir())

        shellcore.mkdir(self.appdir())
        shellcore.copy(self.pkg_file_abspath(), self.path_bin())
        os.chmod(self.path_bin(), 0o777)
        self.create_script()
    
        desktop_info = [
            'Name=KDenLive',
            f'Exec={self.path_bin()} $@',
            'Terminal=false',
            'Type=Application',
            'Comment=Vídeo Editor',
            'Categories=Utility;',
        ]

        self.create_desktop_entry(desktop_info)

    def create_script(self):
        lines = [
            f'{self.path_bin()} $@',
        ]

        f_reader = FileReader(self.scriptPath())
        f_reader.write_lines(lines)
        os.chmod(self.script(), 0o777)

    def verify(self) -> bool:
        return shasum.check_sha256(
            self.pkg_file_abspath(),
            'd9d256865f7007bfa32560bb608ccd10b15c31f3b081b13e1e76816dac399bb6',
        )

    def get_installed_files(self) -> list:
        return [
            self.appdir(),
            self.script(),
            self.desktop_entry(self.appname)
        ]


class JavaDevelopmentKitTarGz(Package):

    # https://jdk.java.net/java-se-ri/15
	# https://openjdk.java.net/install
    # https://www.blogopcaolinux.com.br/2017/06/Como-instalar-o-Oracle-Java-JDK-no-Debian.html

    def __init__(self, *, type_root: bool = False, appname: str):
        super().__init__(type_root=type_root, appname=appname)
        self.dir_project = None

    @property
    def dir_project(self) -> str:
        return self._dir_project

    @dir_project.setter
    def dir_project(self, new_dir: str):
        if new_dir is None:
            return
        if os.path.isdir(new_dir):
            self._dir_project = new_dir

    def version(self) -> str:
        return '15.36'

    def verify(self) -> bool:
        return shasum.check_sha256(self.pkg_file_abspath(), self.pkg_hash_value)
            
    def install(self) -> bool:
        self.chdir()
        os.system('./install-jdk -i')

    def download(self) -> bool:
        self.chdir()
        os.system('./install-jdk -d')
    def uninstall(self) -> bool:
        self.chdir()
        os.system('./install-jdk -u')


class NetBeans(Package):
    """Instalar o NetBeans no Linux"""

    """
    DOWNLOADS:
        https://netbeans.apache.org/download/nb126/nb126.html

    ARQUIVOS:
        https://dlcdn.apache.org/netbeans/netbeans/12.0/netbeans-12.0-bin.zip
        https://dlcdn.apache.org/netbeans/netbeans/12.0/netbeans-12.0-bin.zip.asc

        https://dlcdn.apache.org/netbeans/netbeans/21/netbeans-21-bin.zip

    PUB KEY:
        https://downloads.apache.org/netbeans/KEYS

    
    """

    def __init__(self, *, type_root: bool = False, appname: str):
        super().__init__(type_root=type_root, appname=appname)
        self.pkg_file_name = 'netbeans-21-bin.zip'
        self.url = 'https://dlcdn.apache.org/netbeans/netbeans/21/netbeans-21-bin.zip'
        self.install_jdk15 = JavaDevelopmentKitTarGz(type_root=False, appname='jdk-15')

    def verify(self) -> bool:
        return shasum.check_sha256(self.pkg_file_abspath(),self.pkg_hash_value)

    def version(self) -> str:
        return '12'

    def install(self) -> bool:
        self.install_jdk15.dir_project = self.dir_project
        self.chdir()
        os.system('./install-netbeans -i')

    def download(self) -> bool:
        self.install_jdk15.dir_project = self.dir_project
        self.chdir()
        os.system('./install-netbeans -d')

    def uninstall(self) -> bool:
        self.install_jdk15.dir_project = self.dir_project
        self.chdir()
        os.system('./install-netbeans -u')



class PycharmCommunityTarGz(Package):
    # https://www.jetbrains.com/pt-br/pycharm/download/download-thanks.html?platform=linux&code=PCC
    # f5dd6e642ee65fa96d0ea8447d4a75589ce4258434222c4d9df00d1e5a46a8f5 *pycharm-community-2021.3.1.tar.gz

    def __init__(self, *, type_root: bool = False, appname: str):
        super().__init__(type_root=type_root, appname=appname)
        #self.url = 'https://download-cdn.jetbrains.com/python/pycharm-community-2021.3.1.tar.gz'
        self.url = 'https://download.jetbrains.com/python/pycharm-community-2024.1.tar.gz'
        self.pkg_file_name = 'pycharm-community-2024.1.tar.gz'
        self.pkg_class = 'Desenvolvimento'
        self.pkg_hash_value = '96048ea55ef45f1af0ebe12ad0e38e2763c9e5d654b9e174b33aaa12665d836b'

    def verify(self) -> bool:
        return shasum.check_sha256(self.pkg_file_abspath(), self.pkg_hash_value)

    def install(self) -> bool:
        if os.path.isdir(self.appdir()):
            shellcore.rmdir(self.appdir())

        unpack(self.pkg_file_abspath(), output_dir=self.get_temp_dir(create=True), verbose=True)
        os.chdir(self.get_temp_dir())
        
        for f in os.listdir('.'):
            if 'pycharm-community' in f:
                shellcore.copy(f, self.appdir())
                break

        os.chdir(self.appdir())
        shellcore.rmdir(self.get_temp_dir(create=True))
        shellcore.copy('./bin/pycharm.svg', self.icon('pycharm.svg'))
        self.create_script()

        info: list = [
            "Name=Pycharm Community",
            f"Version={self.version()}",
            f"Icon={self.icon('pycharm.svg')}",
            f"Exec={self.appdir()}/bin/pycharm.sh",
            "Terminal=false",
            "Categories=Development;IDE;",
            "Type=Application",
        ]

        self.create_desktop_entry(info)
        os.chmod(f'{self.appdir()}/bin/pycharm.sh', 0o755)
        os.chmod(self.desktop_entry(self.appname), 0o755)
        os.chmod(self.script(), 0o755)

    def create_script(self):
        f_reader = FileReader(self.scriptPath())
        f_reader.write_lines(["#!/usr/bin/env bash", f'{self.appdir()}/bin/pycharm.sh'])

    def get_installed_files(self) -> list:
        return [
            self.appdir(),
            self.icon('pycharm.svg'),
            self.desktop_entry(self.appname),
            self.script(),
        ]

    def version(self) -> str:
        return "2021.3.1"


class Qgis(Package):
    def __init__(self, *, type_root: bool = False, appname: str, url: str = None, pkg_file_name: str = None):
        super().__init__(type_root=type_root, appname=appname, url=url, pkg_file_name=pkg_file_name)
        #
        # https://www.qgis.org/pt_BR/site/forusers/alldownloads.html#debian-ubuntu
        # 
        self.url = ''
        self.pkg_file_name = ''
        self.pkg_class = 'Outros'

    def download(self) -> bool:
        return True
    
    def verify(self) -> bool:
        return True

    def import_qgis_key(self):
        
        # Baixar a chave de importação
        # sudo wget -O /etc/apt/keyrings/qgis-archive-keyring.gpg https://download.qgis.org/downloads/qgis-archive-keyring.gpg
        file_source_list = '/etc/apt/sources.list.d/qgis.sources'
        file_key = self.system_dirs.tempFile(False)
        url_key = 'https://download.qgis.org/downloads/qgis-archive-keyring.gpg'
        tmp_fpath_source_list: FilePath = FilePath('/tmp/qgis-list')
        f_reader = FileReader(tmp_fpath_source_list)

        _codename = None
        if linux_info.id() == 'linuxmint':
            _codename = linux_info.distro_info('UBUNTU_CODENAME')
        elif linux_info.id() == 'ubuntu':
            _codename = linux_info.codename()
        elif linux_info.id() == 'debian':
            _codename = linux_info.codename()
        else: 
            return False
        
        _content = [
            'Types: deb deb-src',
            'URIs: https://qgis.org/debian',
            f'Suites: {_codename}',
            'Architectures: amd64',
            'Components: main',
            'Signed-By: /etc/apt/keyrings/qgis-archive-keyring.gpg',
        ]
        
        print(f'Baixando ... {file_key}')
        download_manager.download_file(url_key, file_key)
        os.system('sudo mkdir -m755 -p /etc/apt/keyrings')
        os.system(f'sudo cp {file_key} /etc/apt/keyrings/qgis-archive-keyring.gpg')
        f_reader.write_lines(_content)
        os.system(f'sudo cp {tmp_fpath_source_list.absolute()} {file_source_list}')
        return True

    def install(self):

        if not linux_info.distro_base() == 'debian':
            print('Seu sistema não é suportado')
            return False
    
        os.system('sudo apt install gnupg software-properties-common')
        print_line()
        if not self.import_qgis_key():
            return False
        
        os.system('sudo apt update')
        os.system('sudo apt install qgis qgis-plugin-grass')


class Storecli(Package):
    def __init__(self, *, type_root: bool = False, appname: str, url: str = None, pkg_file_name: str = None):
        super().__init__(type_root=type_root, appname=appname, url=url, pkg_file_name=pkg_file_name)
        self.url = 'https://gitlab.com/bschaves/storecli/-/archive/main/storecli-main.zip'
        self.pkg_file_name = 'storecli-main.zip'

    def verify(self) -> bool:
        return True

    def install(self) -> bool:
        unpack(self.pkg_file_abspath(), output_dir=self.get_temp_dir(create=True), verbose=True)
        os.chdir(self.get_temp_dir())
        os.chdir('storecli-main')
        os.chmod('setup.sh', 0o775)
        os.system('./setup.sh')

        #shellcore.copy('tartube-2.3.332', self.appdir())    

class SublimeTextDebFile(PackageDebian):
    def __init__(self, *, type_root: bool = False, appname: str):
        super().__init__(type_root=type_root, appname=appname)
        self.url = 'https://download.sublimetext.com/sublime-text_build-4169_amd64.deb'
        self.pkg_file_name = 'sublime-text.deb'

class SublimeTextTarGz(Package):
    def __init__(self, *, type_root: bool = False, appname: str):
        # https://download.sublimetext.com/sublime_text_build_4126_x64.tar.xz
        super().__init__(type_root=type_root, appname=appname)
        self.url = 'https://download.sublimetext.com/sublime_text_build_4126_x64.tar.xz'
        self.pkg_file_name = 'sublime_text_build_4126_x64.tar.xz'
        self.pkg_class = 'Desenvolvimento'

    def version(self) -> str:
        return '4126'

    def verify(self) -> bool:
        """Verifica integridade com sha256sum"""
        return shasum.check_sha256(
                                    self.pkg_file_abspath(), 
                                    '5c64e534cce0032e54d3c7028e8d6b3bdef28f3fd28a26244a360a2ce75450a1')

    def get_installed_files(self) -> list:
        return [
            self.appdir(),
            self.desktop_entry('sublime-text'),
            self.icon('sublime-text.png'),
            self.script(),
            ]

    def install(self) -> bool:
        if os.path.isdir(self.appdir()):
            shellcore.rmdir(self.appdir())

        unpack(self.pkg_file_abspath(), output_dir=self.get_temp_dir(create=True))
        os.chdir(self.get_temp_dir())
        shellcore.copy('sublime_text', self.appdir())
        os.chdir(self.appdir())
        shellcore.copy('./Icon/128x128/sublime-text.png', self.icon('sublime-text.png'))
        shellcore.rmdir(self.get_temp_dir())

        info: list = [
            f"Version={self.version()}",
            "Type=Application",
            "Name=Sublime Text",
            "GenericName=Text Editor",
            "Comment=Sophisticated text editor for code, markup and prose",
            f"Exec={self.appdir()}/sublime_text %F",
            "Terminal=false",
            "MimeType=text/plain;",
            f"Icon={self.icon('sublime-text.png')}",
            "Categories=TextEditor;Development;",
            "StartupNotify=true",
            "Actions=new-window;new-file;",
            " ",
            "[Desktop Action new-window]",
            "Name=New Window",
            f"Exec={self.appdir()}/sublime_text --launch-or-new-window",
            "OnlyShowIn=Unity;",
            " ",
            "[Desktop Action new-file]",
            "Name=New File",
            f"Exec={self.appdir()}/sublime_text --command new_file",
            "OnlyShowIn=Unity;",
        ]

        self.create_desktop_entry(info)
        self.create_script()

    def create_script(self):

        lines_script = [
            "#!/usr/bin/env bash",
            " ",
            f"{self.appdir()}/sublime_text %F",
        ]

        file_reader = FileReader(self.scriptPath())
        file_reader.write_lines(lines_script)
        os.chmod(self.script(), 0o777)

class BuildSublimeText(object):
    def __init__(self) -> None:
        self._appname = 'sublime-text'
        self.os_base = linux_info.id()

    def build(self) -> Package:
        _app: Package = None
        if self.os_base == 'debian':
            _app = SublimeTextDebFile(type_root=True, appname=self._appname)
        else:
            _app = SublimeTextTarGz(type_root=False, appname=self._appname)
        return _app

class TarTubeTarGz(Package):
    """Gui para download de vídeos do youtube."""

    # https://github.com/axcore/tartube
    #

    def __init__(self, *, type_root: bool = False, appname: str):
        super().__init__(type_root=type_root, appname=appname)
        self.url = 'https://github.com/axcore/tartube/archive/refs/tags/v2.3.332.tar.gz'
        self.pkg_file_name = 'tarbube-v2.3.332.tar.gz'

    def verify(self) -> bool:
        return shasum.check_sha512(
             self.pkg_file_abspath(),
            'd6194cb5e78d541293fecd9ab3847787945585f30b74e5155de017c91fed575c1c947caedd21487e49e68bbd216cea4ae187a368ec482f254c169aa55f6cf51e'
         )

    def version(self) -> str:
        return '2.3.332'

    def install(self) -> bool:

        if os.path.isdir(self.appdir()):
            shellcore.rmdir(self.appdir())

        unpack(self.pkg_file_abspath(), output_dir=self.get_temp_dir(create=True), verbose=True)
        os.chdir(self.get_temp_dir())
        shellcore.copy('tartube-2.3.332', self.appdir())
        os.chdir(self.appdir())
        shellcore.copy('./icons/win/system_icon_256.png', self.icon('tartube.png'))
        self.create_script()
        shellcore.rmdir(self.get_temp_dir())

        info: list = [
            "Name=TarTube",
            f"Version={self.version()}",
            f"Icon={self.icon('tartube.png')}",
            f"Exec={self.script()}",
            "Terminal=false",
            "Categories=Internet;",
            "Type=Application",
        ]

        self.create_desktop_entry(info)

    def create_script(self):
        lines = [
            "#!/usr/bin/env bash",
            " ",
            f"cd ~/",
            f"{self.appdir()}/tartube/tartube",
        ]

        f_reader = FileReader(FilePath(self.script()))
        f_reader.write_lines(lines)
        os.chmod(self.script(), 0o777)
        os.chmod(f"{self.appdir()}/tartube/tartube", 0o777)

    def get_installed_files(self) -> list:
        return [
            self.appdir(),
            self.script(),
            self.icon('tartube.png'),
            self.desktop_entry(self.appname),
        ]



class VeracryptTarFile(Package):
    def __init__(self, *, type_root=False, appname: str):
        super().__init__(type_root=type_root, appname=appname)
        """
        DOWNLOADS:
            https://www.veracrypt.fr/en/Downloads.html

        ARQUIVOS:
            https://launchpadlibrarian.net/572125927/veracrypt-1.25.4-setup.tar.bz2
            https://launchpadlibrarian.net/574400522/veracrypt-1.25.4-sha256sum.txt
            https://launchpadlibrarian.net/574400604/veracrypt-1.25.4-sha512sum.txt

        SIGNATURE:
            https://launchpadlibrarian.net/572125928/veracrypt-1.25.4-setup.tar.bz2.sig


        SHA256:
            2c63e2b1c78058c43c53fd537ab01fc2f1bdc094c9cd64650493e13cbfc503b9  veracrypt-1.25.4-setup.tar.bz2

        SHA512:
            49c63cc553a778d1102641f57a0da493acfe862573ac452ae68f98795806fbcbf8c8c6998b3cd52c62377ab5524f220795737125e057ea8ad29670aaa083de4a  veracrypt-1.25.4-setup.tar.bz2

        PUB KEY:
            https://www.idrix.fr/VeraCrypt/VeraCrypt_PGP_public_key.asc

        Chechar  fingerprint
            gpg --import --import-options show-only VeraCrypt_PGP_public_key.asc
            5069A233D55A0EEB174A5FC3821ACD02680D16DE

        Importar public key
            gpg --import VeraCrypt_PGP_public_key.asc

        Verificar a assinatura do arquivo de instalação.
            gpg --verify veracrypt-1.23-setup.tar.bz2.sig veracrypt-1.23-setup.tar.bz2

        REQUERIMENTOS:
            libgtk-x11-2.0.so.0 (ArchLinux)
            sudo pacman -S gtk2

            xterm
            sudo apt install xterm
            sudo pacman -S xterm

        """

        self.pkg_file_name = 'veracrypt-1.24-Update7-setup.tar.bz2'
        #self.url = 'https://launchpad.net/veracrypt/trunk/1.24-update7/+download/veracrypt-1.24-Update7-setup.tar.bz2'
        self.url = 'https://launchpadlibrarian.net/572125927/veracrypt-1.25.4-setup.tar.bz2'
        self.pkg_class = 'Acessórios'

    def verify(self) -> bool:
        
        return shasum.check_sha256(self.pkg_file_abspath(), 
                                   '2c63e2b1c78058c43c53fd537ab01fc2f1bdc094c9cd64650493e13cbfc503b9'
                                )

    def install(self) -> bool:
        if linux_info.id() == 'archlinux':
            print_line()
            print('Instalando ... gtk2')
            os.system('sudo pacman -S gtk2')

        if not is_executable('xterm'):
            # Instalar xterm.
            print_line()
            print('Necessário intalar xterm')
            if linux_info.id() == 'archlinux':
                os.system('sudo pacman -S xterm')
            elif linux_info.distro_base() == 'debian':
                os.system('sudo apt install xterm --yes')
            elif linux_info.distro_base() == 'fedora':
                os.system('sudo dnf install xterm')

        unpack(self.pkg_file_abspath(), output_dir=self.get_temp_dir(create=True), verbose=True)
        os.chdir(self.get_temp_dir())

        for f in os.listdir('.'):
            if 'setup-gui-x64' in f:
                os.chmod(f, 0o777)
                os.system(f'bash {f}')
        shellcore.rmdir(self.get_temp_dir())

    def uninstall(self):
        #os.system('sudo veracrypt-uninstall.sh')
        return sudo_command(['veracrypt-uninstall.sh'])

    def download(self) -> bool:
        return super().download()

    def is_installed(self) -> bool:
        return is_executable('veracrypt')


class VirtualBoxExtensionPack(Package):
    def __init__(self, *, type_root: bool = False, appname: str):
        super().__init__(type_root=type_root, appname=appname)
        #self.url = 'https://download.virtualbox.org/virtualbox/6.1.30/Oracle_VM_VirtualBox_Extension_Pack-6.1.30.vbox-extpack'
        self.pkg_file_name = 'Oracle_VM_VirtualBox_Extension_Pack-7.0.14.vbox-extpack'
        self.url = 'https://download.virtualbox.org/virtualbox/7.0.14/Oracle_VM_VirtualBox_Extension_Pack-7.0.14.vbox-extpack'
        self.pkg_class = 'Sistema'

    def install(self) -> bool:
        """Instalar o virtualbox extension pack"""
        print_line('-')
        print(f'Executando ... VBoxManage extpack install --replace {self.pkg_file_abspath()}')
        os.system(f'sudo VBoxManage extpack install --replace {self.pkg_file_abspath()}')

        print_line('=')
        print(f'Adicionando o usuário {getuser()} ao grupo vboxusers')
        os.system(f'sudo usermod -a -G vboxusers {getuser()}')

    def verify(self) -> bool:
        return True

class SetUrlVirtualBox(object):
    def __init__(self) -> None:
        self._url_vbox_run = 'https://download.virtualbox.org/virtualbox/7.0.14/VirtualBox-7.0.14-161095-Linux_amd64.run'

    def _set_url_debian(self) -> str:
        
        if linux_info.version_id() == '11':
            return 'https://download.virtualbox.org/virtualbox/6.1.30/virtualbox-6.1_6.1.30-148432~Debian~bullseye_amd64.deb'
        elif linux_info.version_id() == '12':
            return 'https://download.virtualbox.org/virtualbox/7.0.14/virtualbox-7.0_7.0.14-161095~Debian~bookworm_amd64.deb'
        else:
            return self._url_vbox_run

    def _set_url_ubuntu(self) -> str:
        return self._url_vbox_run

    def _set_url_linuxmint(self) -> str:
        return self._url_vbox_run

    def get_url(self) -> str:
        
        if linux_info.id() == 'debian':
            return self._set_url_debian()
        elif linux_info.id() == 'ubuntu':
            return self._set_url_ubuntu()
        elif linux_info.id() == 'linuxmint':
            return self._set_url_linuxmint()
        else:
            return self._url_vbox_run


class VirtualBoxDeb(PackageDebian):
    # https://www.virtualbox.org/wiki/Linux_Downloads

    def __init__(self, *, type_root: bool = False, appname: str):
        super().__init__(type_root=type_root, appname=appname)
        self.pkg_class = 'Sistema'

        self.url = SetUrlVirtualBox().get_url()
        self.pkg_file_name = f'virtualbox-{linux_info.id()}-{linux_info.codename()}.deb'
        self.pkg_online_hash = '547f742d0ad81f4d6b6b46dd5a774c04daee33b911c6130b7f5f3249a437f281'
    
    def verify(self) -> bool:
        return True

    def install_deps(self):
        print_line('-')
        print('Instalando ... module-assistant build-essential libsdl-ttf2.0-0 dkms')
        os.system(f'sudo apt install module-assistant build-essential libsdl-ttf2.0-0 dkms')
        print_line('-')
        print('Instalando ... linux-headers-$(uname -r)')
        os.system(f'sudo apt install linux-headers-$(uname -r)')
        os.system('sudo m-a prepare')

    def install(self) -> bool:
        self.install_deps()
        print_line()
        os.system(f'sudo apt install {self.pkg_file_abspath()}')
        
    def uninstall(self) -> bool:
        return super().uninstall()

    def download(self) -> bool:
        return super().download()



class VirtualBoxAdditionsLinux(Package):
    """
    Instalar virtualbox additions em qualquer distribuição linux
    com o arquivo VBoxLinuxAdditions.run
    """

    # https://www.virtualbox.org/wiki/Downloads
    # https://www.blogopcaolinux.com.br/2017/08/Instalando-Adicionais-para-Convidado-no-Debian.html
    # https://askubuntu.com/questions/22743/how-do-i-install-guest-additions-in-a-virtualbox-vm

    def __init__(self, *, type_root:bool=False, appname: str):
        super().__init__(type_root=type_root, appname=appname)
        self.url = 'https://download.virtualbox.org/virtualbox/6.1.26/VBoxGuestAdditions_6.1.26.iso'
        self.pkg_file_name = 'VBoxGuestAdditions_6.1.26.iso'
        self.pkg_class = 'Sistema'

    def verify(self) -> bool:
        return shasum.check_sha256(
            self.pkg_file_abspath(),
            '22d02ec417cd7723d7269dbdaa71c48815f580c0ca7a0606c42bd623f84873d7'
        )


    def _exec_vboxlinux_additions(self) -> bool:
        """
           Monta a ISO VBoxGuestAdditions.iso em um diretório temporário, e executa
        o arquivo VBoxLinuxAdditions.run
        """

        # /opt/VirtualBox/additions/VBoxGuestAdditions.iso
        # /usr/share/virtualbox/VBoxGuestAdditions.iso
        # sudo mount -o loop /tmp/VBoxGuestAdditions_5.0.20.iso /mnt

        """
        if os.path.isfile('/opt/VirtualBox/additions/VBoxGuestAdditions.iso'):
            guest_iso = '/opt/VirtualBox/additions/VBoxGuestAdditions.iso'
        elif os.path.isfile('/usr/share/virtualbox/VBoxGuestAdditions.iso'):
            guest_iso = '/usr/share/virtualbox/VBoxGuestAdditions.iso'
        else:
            print(f'{__class__.__name__} ERRO arquivo VBoxGuestAdditions.iso não encontrado.')
            return False
        """

        file_additions = f'{self.get_temp_dir(create=True)}/VBoxLinuxAdditions.run'
        print(f'Montando ... {self.pkg_file_abspath()}')
        os.system(f'sudo mount -o loop {self.pkg_file_abspath()} {self.get_temp_dir()}')
        print_line()
        print(f'Executando ... {file_additions}')
        os.system(f'sudo {file_additions}')

        # Desmontar o dispositivo montado anteriormente.
        for k in get_mount_dirs():
            if get_mount_dirs()[k] == self.get_temp_dir():
                print(f'Desmontando ... {k} => {self.get_temp_dir()}')
                os.system(f'sudo umount {k}')


        return sudo_command('rm', '-rf', self.get_temp_dir())
        #os.system(f'sudo rm -rf {self.get_temp_dir()}')

    def archlinux_additions(self) -> bool:
        os.system('sudo pacman -S virtualbox-guest-utils')
        os.system('sudo systemctl enable vboxservice.service')
        os.system('sudo modprobe -a vboxguest vboxsf vboxvideo')
        return True

    def ubuntu_focal(self) -> bool:
        os.system('sudo apt update')
        print_line('=')
        print('Instalando ... módulos do kernel')
        print_line()
        os.system('sudo apt install linux-headers-$(uname -r)')
        os.system('sudo apt install build-essential module-assistant dkms')
        os.system('sudo m-a prepare')
        #os.system('sudo apt install virtualbox-guest-modules')
        return self._exec_vboxlinux_additions()

    def linuxmint_debian_edition(self) -> bool:
        os.system('sudo apt update')
        print_line('=')
        print('Instalando ... build-essential module-assistant dkms linux-headers-$(uname -r)')
        print_line('=')
        os.system('sudo apt install build-essential module-assistant dkms')
        os.system('sudo apt install linux-headers-$(uname -r)')
        os.system('sudo m-a prepare')
        os.system('sudo apt install virtualbox-guest-x11')
        os.system('sudo apt install virtualbox-guest-additions-iso')
        os.system('sudo dpkg --configure -a')
        return self._exec_vboxlinux_additions()

    def fedora_additions(self) -> bool:
        """
        Pacotes requeridos:
        gcc kernel-devel kernel-headers dkms make bzip2 perl libxcrypt-compat

        $(rpm -qa kernel | sort -V | tail -n 1)
        kernel-devel-$(uname -r)
        virtualbox-guest-additions
        """
        print_line()
        print('Instalando ... gcc kernel-devel kernel-headers dkms make bzip2 perl libxcrypt-compat')
        print_line()
        os.system('sudo dnf install gcc kernel-devel kernel-headers dkms make bzip2 perl libxcrypt-compat')

        print_line()
        print('Instalando ... $(rpm -qa kernel | sort -V | tail -n 1) kernel-devel-$(uname -r) virtualbox-guest-additions')
        print_line()

        os.system('sudo dnf install $(rpm -qa kernel | sort -V | tail -n 1)')
        os.system('sudo dnf install kernel-devel-$(uname -r)')
        os.system('sudo dnf install virtualbox-guest-additions')
        #return self._exec_vboxlinux_additions()
        return True

    def install(self) -> bool:

        if linux_info.id() == 'arch':
            return self.archlinux_additions()
        elif linux_info.id() == 'fedora':
            self.fedora_additions()
        elif linux_info.id() == 'debian':
            return self.linuxmint_debian_edition()
        elif (linux_info.id() == 'linuxmint'):
            if linux_info.codename() == 'debbie':
                return self.linuxmint_debian_edition()
            elif linux_info.distro_info('ID')[0:2] == '20':
                return self.ubuntu_focal()

        return False



class VirtualBox(Package):
    def __init__(self, *, type_root: bool = False, appname: str):
        super().__init__(type_root=type_root, appname=appname)
        self.pkg_class = 'Sistema'

        if linux_info.distro_base() == 'debian':
            self.app: Package = VirtualBoxDeb(appname=self.appname)
        else:
            self.app = None

    def verify(self) -> bool:
        if self.app is None:
            return False
        return self.app.verify()

    def download(self) -> bool:
        if self.app is None:
            return False
        
        return self.app.download()
    
    def configure_vboxdrv(self):
        
        if os.path.isfile('/etc/init.d/vboxdrv'):
            print('Executando /etc/init.d/vboxdrv setup')
            os.system('sudo /etc/init.d/vboxdrv setup')
        elif os.path.isfile('/usr/lib/virtualbox/vboxdrv.sh'):
            print('Executando sudo /usr/lib/virtualbox/vboxdrv.sh setup')
            os.system('sudo /usr/lib/virtualbox/vboxdrv.sh setup')


    def install(self) -> bool:
        if self.app is None:
            return False
        self.app.install()
        self.configure_vboxdrv()
        os.system('echo Adicionando $USER ao grupo vboxusers')
        os.system('sudo usermod -a -G vboxusers $USER')

    def uninstall(self) -> bool:
        if self.app is None:
            return False
        return self.app.uninstall()


class VsCodeDebFile(PackageDebian):

    def __init__(self, *, type_root: bool = False, appname: str):
        super().__init__(type_root=type_root, appname=appname)
        self.pkg_file_name = 'vscode.deb'
        self.pkg_class = 'Desenvolvimento'
        self.url = 'https://vscode.download.prss.microsoft.com/dbazure/download/stable/8b3775030ed1a69b13e4f4c628c612102e30a681/code_1.85.2-1705561292_amd64.deb'

    def verify(self) -> bool:
        return True

class VsCodeTarGz(Package):
    # https://code.visualstudio.com/sha/download?build=stable&os=linux-x64
    # code-stable-x64.tar.gz
    #

    def __init__(self, *, type_root: bool = False, appname: str):
        super().__init__(type_root=type_root, appname=appname)
        self.url='https://vscode.download.prss.microsoft.com/dbazure/download/stable/8b3775030ed1a69b13e4f4c628c612102e30a681/code-stable-x64-1705560028.tar.gz'
        self.pkg_file_name='code-stable-x64.tar.gz'
        self.pkg_class = 'Desenvolvimento'

    def verify(self) -> bool:
        return True

    def install(self) -> bool:
        if os.path.isdir(self.appdir()):
            shellcore.rmdir(self.appdir())

        unpack(self.pkg_file_abspath(), output_dir=self.get_temp_dir(create=True), verbose=True)
        os.chdir(self.get_temp_dir())
        shellcore.copy('VSCode-linux-x64', self.appdir())
        os.chdir(self.appdir())
        shellcore.copy('./resources/app/resources/linux/code.png', self.icon('code.png'))
        self.create_script()

        info = [

            "Name=Visual Studio Code",
            "Comment=Code Editing. Redefined.",
            "GenericName=Text Editor",
            f"Exec={self.appdir()}/bin/code --unity-launch %F",
            f"Icon={self.icon('code.png')}",
            "Type=Application",
            "StartupNotify=false",
            "StartupWMClass=Code",
            "Categories=Utility;TextEditor;Development;IDE;",
            "MimeType=text/plain;inode/directory;application/x-code-workspace;",
            "Actions=new-empty-window;",
            "Keywords=vscode;",
            " ",
            "[Desktop Action new-empty-window]",
            "Name=New Empty Window",
            f"Exec={self.appdir()}/bin/code --new-window %F",
            f"Icon={self.icon('code.png')}",
        ]

        self.create_desktop_entry(info)
        os.chmod(self.desktop_entry(self.appname), 0o777)
        shellcore.rmdir(self.get_temp_dir())

    def get_installed_files(self) -> list:
        return [
            self.appdir(),
            self.desktop_entry(self.appname),
            self.icon('code.png'),
            self.script(),
        ]

    def create_script(self):
        #FileReader(self.scriptPath()).write_lines(['#!/usr/bin/env bash', f'{self.appdir()}/bin/code $@'])
        os.chmod(f'{self.appdir()}/bin/code', 0o777)
        os.system(f'ln -sf {self.appdir()}/bin/code {self.script()}')


class BuildVsCode(object):
    def __init__(self):
        if (linux_info.id() == 'debian') or (linux_info.id() == 'ubuntu'):
            self._app_vscode: PackageDebian = VsCodeDebFile(appname='code')
        else:
            self._app_vscode: Package = VsCodeTarGz(appname='code')

    def build(self) -> Package:
        return self._app_vscode

class YoutubeDl(Package):
    def __init__(self, *, type_root: bool = False, appname: str, url: str = None, pkg_file_name: str = None):
        super().__init__(type_root=type_root, appname=appname, url=url, pkg_file_name=pkg_file_name)
        # https://github.com/ytdl-org/youtube-dl
        
        self.url = 'https://github.com/ytdl-org/youtube-dl/archive/refs/heads/master.zip'
        self.pkg_file_name = 'youtube-dl.zip'

    def verify(self) -> bool:
        return True
    
    def install(self) -> bool:
        unpack(self.pkg_file_abspath(), output_dir=self.get_temp_dir(create=True), verbose=True)
        os.chdir(self.get_temp_dir())
        os.chdir('youtube-dl-master')
        os.system(f'{sys.executable} setup.py install --user')

    def uninstall(self) -> bool:
        os.system(f'{sys.executable} -m pip uninstall youtube-dl')


class WineTricks(Package):
    def __init__(self, *, type_root: bool = False, appname: str):
        super().__init__(type_root=type_root, appname=appname)
        self.url = 'https://github.com/Winetricks/winetricks/archive/refs/tags/20211230-test.zip'
        self.pkg_file_name = 'winetricks-20211230-test.zip'
        self.pkg_class = 'Outros'

    def install_deps(self):
        """
           Instalar as dependências do winetricks
        """

        if linux_info.distro_base() == 'debian':
            print('Instalando ... wget binutils cabextract fuseiso p7zip-full policykit-1 unrar unzip xdg-utils xz-utils zenity')
            os.system('sudo apt install -y wget binutils cabextract fuseiso p7zip-full policykit-1 unrar unzip xdg-utils xz-utils zenity')
        else:
            pass

    def verify(self) -> bool:
        return shasum.check_sha256(self.pkg_file_abspath(), 'fb067d5a047ceb9fcfe6176fb6af3f97f2560efdc7efe65e0f88cbc6e5591115')

    def install(self) -> bool:
        self.install_deps()

        unpack(self.pkg_file_abspath(), output_dir=self.get_temp_dir())
        os.chdir(self.get_temp_dir())
        for _dir in os.listdir('.'):
            if 'winetricks' in _dir:
                os.chdir(_dir)
                break
        os.chdir('./src')
        shellcore.copy('winetricks', self.script())
        #shellcore.copy('winetricks.desktop', self.desktop_entry(self.appname))
        shellcore.copy('winetricks.svg', self.icon('winetricks.svg'))
        shellcore.rmdir(self.get_temp_dir())

        info = [
            "Name=Winetricks",
            "Comment=Work around problems and install applications under Wine",
            "Comment[pl]=Rozwiązuj problemy i instaluj aplikacje z użyciem Wine",
            "Comment[ru]=Решение проблем и установка программ с помощью Wine",
            "Exec=winetricks --gui",
            "Terminal=false",
            f"Icon={self.icon('winetricks.svg')}",
            "Type=Application",
            "Categories=Utility;",
        ]

        self.create_desktop_entry(info)
        os.chmod(self.script(), 0o777)
        os.chmod(self.desktop_entry('winetricks'),  0o777)

    def get_installed_files(self) -> list:
        return [
            self.icon('winetricks.svg'),
            self.script(),
            self.desktop_entry('winetricks'),
        ]

    def is_installed(self) -> bool:
        return is_executable('winetricks')

    def get_info(self) -> dict:
        return {
            "url": self.url,
            "arquivo desktop": self.desktop_entry('winetricks'),
            "arquivo de instalação": self.pkg_file_abspath(),
            "icone": self.icon('winetricks.svg'),
            "script": self.script(),
            "categoria": self.pkg_class,
            "instalado": self.is_installed(),
        }


class GetApps(object):
    def __init__(self) -> None:
        super().__init__()
        self.__apps_list: list = []

    def set_apps_linux(self) -> None:

        #atom = AtomDebFile(appname='atom')
        atom = AtomTarGz(appname='atom')
        android_studio = AndroidStudioTarGz(appname='android-studio')
        google_chrome = GoogleChrome(appname='google-chrome')
        electrum_wallet = ElectrumAppImage(appname='electrum-wallet')
        etcher = BalenaEtcherAppimage(appname='balena-etcher')
        flutter = Flutter(appname='flutter')
        genymotion = Genymotion(appname='genymotion')
        java_development_kit = JavaDevelopmentKitTarGz(appname='jdk-15')
        kdenlive = KdenLiveAppImage(appname='kdenlive')
        netbeans = NetBeans(appname='netbeans')
        pycharm_community = PycharmCommunityTarGz(appname='pycharm-community')
        qgis = Qgis(appname='qgis')
        storecli = Storecli(appname='storecli')
        sublime = BuildSublimeText().build()
        tartube = TarTubeTarGz(appname='tartube')
        # vscode = VsCodeTarGz(appname='code')
        vscode = BuildVsCode().build()
        winetricks = WineTricks(appname='winetricks')
        veracrypt = VeracryptTarFile(appname='veracrypt')
        vbox_extension_pack = VirtualBoxExtensionPack(appname='virtualbox-extension-pack')
        virtual_box = VirtualBox(appname='virtualbox')
        virtual_box_additions = VirtualBoxAdditionsLinux(appname='virtualbox-additions')
        youtubedl = YoutubeDl(appname='youtube-dl')

        self.__apps_list.append(atom)
        self.__apps_list.append(android_studio)
        self.__apps_list.append(google_chrome)
        self.__apps_list.append(etcher)
        self.__apps_list.append(electrum_wallet)
        self.__apps_list.append(kdenlive)
        self.__apps_list.append(flutter)
        self.__apps_list.append(genymotion)
        self.__apps_list.append(java_development_kit)
        self.__apps_list.append(netbeans)
        self.__apps_list.append(tartube)
        self.__apps_list.append(pycharm_community)
        self.__apps_list.append(qgis)
        self.__apps_list.append(storecli)
        self.__apps_list.append(sublime)
        self.__apps_list.append(veracrypt)
        self.__apps_list.append(vscode)
        self.__apps_list.append(virtual_box)
        self.__apps_list.append(vbox_extension_pack)
        self.__apps_list.append(virtual_box_additions)
        self.__apps_list.append(youtubedl)
        self.__apps_list.append(winetricks)

    def get_app_from_name(self, name: str) -> Package:
        """Retorna um pacote/Package com base no nome."""
        for pkg in self.get_apps():
            if pkg.appname == name:
                return pkg
                break
        return None

    def get_apps_names(self) -> list:
        """
           Retorna os nomes de todos os apps na lista self.get_apps()
        """
        names: list = []
        for pkg in self.get_apps():
            names.append(pkg.appname)
        return sorted(names)

    def get_apps(self) -> list:
        """Retorna uma lista de objetos/Packages"""
        if self.__apps_list == []:
            self.set_apps_linux()
        return self.__apps_list
