#!/usr/bin/env python3
"""
REQUERIMENTO: libappcli.

REPO:
   https://gitlab.com/bschaves/app-cli/-/tree/main
   https://gitlab.com/bschaves/app-cli/-/archive/main/app-cli-main.zip

ONLINE FILES:
   _version.py
   https://gitlab.com/bschaves/app-cli/-/raw/main/appcli/_version.py?inline=false
"""
import sys
import os
import argparse

file_main = os.path.realpath(__file__)
dir_of_project = os.path.abspath(os.path.join(os.path.dirname(file_main), '..'))
sys.path.insert(0, dir_of_project)
    
        
parser = argparse.ArgumentParser(
                description='Instala programas em sistemas Linux.'
                )


def main():
    from appcli.libappcli import PkgsManager, AppcliLocalManager
    from appcli._version import __version__
    
    pkg_manager = PkgsManager()
    pkg_manager.dir_project = dir_of_project
    appcli_manager = AppcliLocalManager()

    #assume_yes: bool = False
    #download_only: bool = False

    parser.add_argument(
        '-v', '--version',
        action='version',
        version=__version__,
    )

    parser.add_argument(
        '-l', '--list',
        action='store_true',
        dest='list_apps',
        help='Mostra pacotes disponíveis para instalação.'
    )

    parser.add_argument(
        '-y', '--yes',
        action='store_true',
        dest='assume_yes',
        help='Assume SIM para todas as indagações.'
    )

    parser.add_argument(
        '-d', '--download-only',
        action='store_true',
        dest='download_only',
        help='Somente baixa os pacotes sem instalar.'
    )

    parser.add_argument(
        '-i', '--install',
        action='store',
        nargs='*',
        dest='pkgs_for_install',
        type=str,
        help='Instalar um ou mais pacotes'
    )

    parser.add_argument(
        '-u', '--uninstall',
        action='store',
        nargs='*',
        dest='pkgs_for_uninstall',
        type=str,
        help='Remover um ou mais pacotes.'
    )

    parser.add_argument(
        '-U', '--update',
        action='store_true',
        dest='update',
        help='Instala a ultima versão do appcli no seu sistema.'
    )

    parser.add_argument(
        '--clean',
        action='store_true',
        dest='clean_apps',
        help='Apaga os arquivos baixados em cache.'
    )

    parser.add_argument(
        '--clean-cache',
        action='store',
        nargs=1,
        dest='clean_cache',
        type=str,
        help='Apaga o cache de um app. --clean-cache <app>'
    )

    parser.add_argument(
        '--info',
        action='store',
        nargs=1,
        dest='pkg_info',
        type=str,
        help='Mostra informações sobre um pacote.'
    )


    args = parser.parse_args()

    if args.assume_yes:
        pkg_manager.assume_yes = True
        appcli_manager.assume_yes = True
    if args.download_only:
        pkg_manager.download_only = True
        
    if args.list_apps:
        pkg_manager.list_apps()
    elif args.pkgs_for_install:
        pkg_manager.install_apps(args.pkgs_for_install)
    elif args.pkgs_for_uninstall:
        pkg_manager.uninstall_apps(args.pkgs_for_uninstall)
    elif args.update:
        appcli_manager.check_uptade()
    elif args.clean_cache:
        pkg_manager.clean_package_cache(args.clean_cache[0])
    elif args.clean_apps:
        pkg_manager.clean_downloads_cache()
    elif args.pkg_info:
        pkg_manager.list_info(args.pkg_info[0])
        
    

if __name__ == '__main__':
    main()




