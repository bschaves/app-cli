#!/usr/bin/env python3
#

"""
  Biblioteca facilitar a instalação de aplicativos em sistemas 
Linux e Windows.


REQUERIMENTOS:
	tqdm >= 4.61.0
	requests >= 2.21.0
	bs4

"""

import json
import os
import sys
import hashlib
import re
import platform
import urllib.request
import shutil

from pathlib import Path
from tempfile import NamedTemporaryFile, TemporaryDirectory
from shutil import (
    which,
    copytree,
    copy,
    copyfile,
    rmtree,
    unpack_archive,
)

from subprocess import (
    PIPE,
    Popen,
    run as subprocess_run
)


try:
	from bs4 import BeautifulSoup
	import requests
	import tqdm
except Exception as e:
	print(e)
	sys.exit(1)


HOME = os.path.abspath(Path().home())
KERNEL_TYPE = platform.system()

if KERNEL_TYPE == 'Linux':
    from os import geteuid


def mkdir(path: str) -> bool:
    """Função para criar um diretório."""
    if path is None:
        return False

    if os.path.exists(path):
        return True
        
    try:
        os.makedirs(path)
    except Exception as e:
        print(__name__, e)
        return False
    else:
        return True

def get_abspath(path: str) -> str:
    """Retorna o caminho absoluto de um arquivo ou diretório."""
    try:
        return os.path.abspath(path)
    except Exception as e:
        print(f'get_abspath ERRO -> arquivo do tipo None é inválido')
    return ''

def touch(file) -> bool:
    try:
        Path(file).touch()
    except:
        return False
    else:
        return True

def get_term_colums() -> int:
    """Retorna o número de colunas do terminal"""
    try:
        _col: int = os.get_terminal_size()[0]
    except:
        _col: int = 60

    return _col


def is_executable(file: str) -> bool:
    if file is None:
        return False
    _exec = shutil.which(file)
    if _exec is None:
        return False
    if not os.path.isfile(_exec):
        return False
    return True
   
# Função para ser usada em sistemas Linux.
def add_home_in_path() -> bool:
    '''
       Configurar o arquivo .bashrc do usuário para inserir o diretório ~/.local/bin
    na variável de ambiente $PATH. 
    
       Essa configuração será abortada caso ~/.local/bin já exista em ~/.bashrc ou exista na variável $PATH.
    '''
    if KERNEL_TYPE != 'Linux':
        print(f'{__name__} ERRO ... sistema operacional não suportado.')
        return False

    if geteuid() == 0:
        print(f'{__name__} ERRO ... você não pode ser o "root."')
        return False
    
    
    confUserDirs: SystemLocalDirs = SystemLocalDirs(type_root=False)

    # Verificar se ~/.local/bin já está no PATH do usuário atual.
    # os.environ
    user_local_path = os.environ['PATH']
    
    for _dir in user_local_path.split(':'):
        if _dir == confUserDirs.dirBin():
            return True
            break

    file_bashrc_backup = confUserDirs.fileBashRc() + '.bak'
    if not os.path.isfile(file_bashrc_backup):
        print(f'Criando backup do arquivo {confUserDirs.fileBashRc()}')
        copyfile(confUserDirs.fileBashRc(), file_bashrc_backup)

    file_bash: FilePath = FilePath(confUserDirs.fileBashRc()) 
    file_reader: FileReader = FileReader(file_bash)
    content: list = file_reader.get_lines()

    import re
    RegExp = re.compile(r'{}.*{}'.format('^export PATH=', confUserDirs.dirBin()))
    for line in content:
        if (RegExp.findall(line) != []): # O arquivo já foi configurado anteriormente.
            print(f'add_home_in_path(): arquivo {confUserDirs.fileBashRc()} já configurado.')
            return True
            break

    # Inserir ~/.local/bin no arquivo ~/.bashrc
    NewUserPath = f'export PATH={confUserDirs.dirBin()}:{user_local_path}'
    content.append(NewUserPath)

    # Escrever a nova lista no arquivo ~/.bashrc
    file_reader.write_lines(content)

    return True


def is_admin() -> bool:
    """
        Verificar se o usuário atual é administrador
    Requerimentos: 
        comando 'id' no linux. (id --user)
    """
    if sys.platform != 'linux':
        return False

    if os.geteuid() == 0:
        return True
    
    # print("Verificando se você é administrador ...", end=' ')
    # sys.stdout.flush()
    # proc = ExecShellCommand(['sudo', 'sh', '-c', 'id --user'])
    # proc.exec_silent()
    # return proc.returnbool


def sudo_command(cmds: list) -> bool:
    """
        Executa comandos com o sudo em sistemas Linux.  
    Ex:
        mkdir /teste

    sudo_command(['mkdir', '/teste'])    
    """
    if not isinstance(cmds, list):
        raise Exception('sudo_command ERRO ... os comandos precisam ser passados em forma de lista.')

    cmds.insert(0, 'sudo')
    print('-' * get_term_colums())
    print('Executando ...', end=' ')
    
    for c in cmds:
        print(c, end=' ')
    print()
    print('-' * get_term_colums())

    proc = ExecShellCommand(cmds)
    proc.exec()
    return proc.returnbool


def get_mount_dirs() -> dict:
    """
        Retorna os diretórios de montagem em um sistema Linux.
    EX:
    sysfs      /sys
    proc       /proc
    /dev/sda2  /boot/efi  
    /dev/sda3  /

    retorno:
      {
          'sysfs': '/sys',
          'proc': '/proc',
          '/dev/sda2': '/boot/efi',
          '/dev/sda3': '/',
      }  
    """

    if KERNEL_TYPE != 'Linux':
        return False

    mount_dirs = {}
    lines: list = FileReader(FilePath('/proc/mounts')).get_lines()
    for line in lines:
        line = line.replace('\n', '')
        mount_dirs.update({line.split()[0]: line.split()[1]})
    return mount_dirs


def device_is_mounted(device: str) -> bool:
    """Verifica se um dispositivo está montado."""

    dev_path = Path(device)
    if not dev_path.is_block_device():
        return False

    if not device in get_mount_dirs().keys():
        return False
    return True
    

class FilePath(object):
    """
       Classe para trabalhar com um arquivo, e obter algumas informações como:

    exists()    - Verificar se o arquivo existe.
    name()      - Retorna o nome do arquivo, sem a extensão. 
    dirname()   - Retorna o diretório PAI do arquivo.
    basename()  - Retorna o nome do arquivo com a extensão.
    extension() - Retorna a extensão do arquivo, com base no nome.
    """
    def __init__(self, filename: str) -> None:
        super().__init__()
        self.filename = filename
        self.__path = None

    def path(self) -> Path:
        """Retorno um objeto do tipo Path para o arquivo atual"""
        if self.__path is None:
            self.__path = Path(self.filename)
        return self.__path

    def absolute(self) -> str:
        """Retorna o caminho absoluto de um arquivo"""
        return get_abspath(self.filename)

    def exists(self) -> bool:
        try:
            if os.path.exists(self.absolute()):
                return True
        except:
            return False

    def name(self) -> str:
        """
           Retorna o nome do arquivo sem a extensão, e sem o caminho absoluto.
        Ex:
           /path/to/file_name.pdf

        name() -> file_name
        """
        if self.extension() is None:
            return self.basename()
        return self.basename().replace(self.extension(), "")
    
    def dirname(self) -> str:
        """
           Retorna o caminho absoluto do diretório pai do arquivo.
        """
        return os.path.dirname(self.absolute())

    def basename(self) -> str:
        """
           Retorna o nome do arquivo com a extensão, sem o caminho absoluto.
        Ex:
           /path/to/file_name.pdf

        basename() -> file_name.pdf
        """
        return os.path.basename(self.absolute())
        
    def drive(self):
        return Path(self.absolute()).drive

    def extension(self) -> str:
        """Retorna a extensão do arquivo baseado no nome"""
        _ext = os.path.splitext(self.absolute())[1]
        if _ext == '': 
            return None
        return _ext
    
    def touch(self) -> None:
        Path(self.absolute()).touch()

    def delete(self) -> None:
        """Deleta o arquivo"""
        try:
            os.remove(self.absolute())
        except Exception as e:
            print(__class__.__name__, e)

    def part_name(self, chars: int) -> str:
        """
            Retorna o nome de arquivo parcialmente.
        part_name(20) - Retorna os primeiros 20 caracteres do nome.
        """
        
        if (len(self.basename()) + 1) < chars:
            return self.basename()
        return self.basename()[0 : chars]


class FileReader(object):
    """
       Classe para ler e escrever linhas em arquivos de texto

    get_lines() - Retorna as linhas de um arquivo em forma de lista.
    write_lines(list) - Recebe uma lista, e grava os dados da lista no arquivo.
    """

    def __init__(self, file_path: FilePath) -> None:
        super().__init__()
        self.file_path: FilePath = file_path

    def get_lines(self) -> list:
        """Retorna uma lista com as linhas do arquivo de texto."""
        lines = []
        try:
            with open(self.file_path.absolute(), 'rt') as file:
                lines = file.readlines()

        except Exception as e:
            print(__class__.__name__, e)
            return lines
        else:
            return lines

    def write_lines(self, lines: list) -> None:
        """
           Sobreescrever um arquivo, gravando o conteúdo de lines no arquivo.
        Todos os dados existentes serão perdidos. Quebras de linha '\n' são 
        inseridas automáticamente no fim de cada linha.
        """
        if not isinstance(lines, list):
            print(__class__.__name__, "ERRO ... lines precisa ser do tipo lista")
            return

        try:
            with open(self.file_path.absolute(), 'w') as file:
                for line in lines:
                    file.write(f'{line}\n')
        except Exception as e:
            print(__class__.__name__, e)

    def append_lines(self, lines: list) -> None:
        """Adiciona o conteúdo de lines no fim do arquivo de texto"""
        if not isinstance(lines, list):
            print(__class__.__name__, "ERRO ... lines precisa ser do tipo lista")
            return

        try:
            with open(self.file_path.absolute(), 'a') as file:
                for line in lines:
                    file.write(f'{line}\n')
        except Exception as e:
            print(__class__.__name__, e)

    def is_text(self, text: str) -> bool:
        """Verifica se text existe no arquivo de texto"""
        if len(self.find_text(text)) > 0:
            return True
        return False
    
    def find_text(self, text: str, *, max_count: int=0, ignore_case:bool=False) -> list:
        """
            Retorna uma lista com todas as ocorrências de text nas linhas do arquivo.
        max_cout = Máximo de ocorrências a buscar no arquivo.
        ignore_case = Ignora o case sensitive.
        """
        _lines = self.get_lines()
        _lst = []
        if _lines == []:
            return []

        for line in _lines:
            if ignore_case:
                if text.lower() in line.lower():
                    _lst.append(line)
            else:
                if text in line:
                    _lst.append(line)

            if (max_count > 0) and (len(_lst) == max_count):
                break
           
        return _lst
  

class FileJson(object):
    """
       Classe com as funcionalidades:
    - Ler um arquivo json e retornar o conteúdo em forma de dicionário.

    - Sobreescrever/Criar um arquivo json apartir de um dicionário. 

    - Alterar/Adicionar o conteúdo de uma chave.

    - Alterar/Adicionar uma chave.

    """
    def __init__(self, file_path_json: FilePath):
        super().__init__()
        self.file_path_json: FilePath = file_path_json

    def write_lines(self, new_lines: dict):
        """
            Recebe um dicionário e escreve/sobreescre o aquivo com base no objeto atual
        """

        if not isinstance(new_lines, dict):
            #print(f'{__class__.__name__} ERRO')
            raise Exception(f'{__class__.__name__} ERRO ... tipo de dados incorreto ... {new_lines}')
            return

        try:
            with open(self.file_path_json.absolute(), 'w', encoding='utf8') as jfile:
                json.dump(new_lines, jfile, ensure_ascii=False, sort_keys=True, indent=4)
        except Exception as e:
            #print(__class__.__name__, e)
            raise Exception(f'{__class__.__name__} {e}')

    def lines_to_dict(self) -> dict:
        """
        Ler o conteúdo do arquivo .json e retornar as linhas em forma de um dicionário
        """
        try:
            with open(self.file_path_json.absolute(), 'rt', encoding='utf8') as jfile:
                content = json.load(jfile)
        except Exception as e:
            print(__class__.__name__, e)
            return {}
        else:
            return content

    def update_key(self, new_key: str, value: str):
        """
          Altera/Cria a chave 'new_key' com o valor 'value'

        Se new_key já existir, será modificada, se não será alterada.
        """
        content = self.lines_to_dict()

        if not new_key in content.keys():
            content.update({new_key: value})
        else:
            content[new_key] = value

        self.write_lines(content)

    def is_key(self, key: str) -> bool:
        """Verifica se uma chave/key existe no json"""
        _keys = self.lines_to_dict().keys()

        for json_key in self.lines_to_dict().keys():
            if json_key == key:
                return True
                break
        return False

    def get_lines(self):
        return json.dumps(self.lines_to_dict(), indent=4, ensure_ascii=False)
        

class PathUserDirs(object):
    """Diretórios para cache, configurações, libs, binários entre outros."""
    def __init__(self, type_root: bool = False) -> None:
        super().__init__()
        self.type_root: bool = type_root
        self.home = os.path.abspath(Path().home())
        self.dir_bin = None
        self.dir_lib = None
        self.dir_icons = None
        self.dir_desktop_entry = None
        self.dir_themes = None
        self.dir_optional = None
        self.dir_cache = None
        self.dir_config = None
        self.dir_gnupg = None
        self.dir_downloads = None
        self.file_bashrc = None
    
    @property
    def type_root(self) -> bool:
        return self._type_root

    @type_root.setter
    def type_root(self, new_type_root) -> None:
        """Proteger atribuições para type_root."""
        if KERNEL_TYPE == 'Windows':
            self._type_root = False
            return
            
        if geteuid() == 0:
            self._type_root = True
            return
        if not isinstance(new_type_root, bool):
            return
        self._type_root = new_type_root

    def getDirs(self) -> dict:
        """
           Retorna todos os diretórios e arquivos de configuração em forma de um dicionário.
        """
        pass

    def setDirs(self) -> None:
        """Atribuir os diretórios para o root ou para user"""
        pass

    def getDirIcons(self, resol: str) -> str:
        """Retorna um diretório para guardar icones."""
        pass

    def showDirs(self):
        """Mostra os diretórios de configuração no STDOUT."""
        pass



class SystemLocalDirsLinux(PathUserDirs):
    """
      Classe para trabalhar com o caminho absoluto de diretório
    para instalação de aplicativos em sistemas Linux.
    """
    def __init__(self, type_root: bool=False) -> None:
        super().__init__(type_root)

        # type_root: se for False esta classe irá trabalhar com as 
        # configurações de usuário padrão, se for True, irá trabalhar
        # com as configurações do usuário root.
        
        if geteuid() == 0:
            # Se o id do usuário for root type_root será True automáticamente.
            self.type_root = True
        else:
            self.type_root = type_root
        
        self.setDirs()

    def setDirs(self):
        
        if self.type_root:
            # Configurações para o root.
            self.home = '/root'
            self.dir_bin = '/usr/local/bin'
            self.dir_lib = '/usr/local/lib'
            self.dir_icons = '/usr/share/icons/hicolor/128x128/apps'
            self.dir_desktop_entry = '/usr/share/applications'
            self.dir_themes = '/usr/share/themes'
            self.dir_optional = '/opt'
            self.dir_cache = '/var/cache'
            self.dir_config = '/etc'
            self.dir_gnupg = '/root/.gnupg'
            self.dir_downloads = '/root/Downloads'
            self.file_bashrc = '/etc/bash.bashrc'
        else:
            # Configurações do usuário padrão.
            self.home = os.path.abspath(Path().home())
            self.dir_bin = get_abspath(os.path.join(self.home, '.local', 'bin'))
            self.dir_lib = get_abspath(os.path.join(self.home, '.local', 'lib'))
            self.dir_icons = get_abspath(os.path.join(self.home, '.local', 'share', 'icons', '128x128'))
            self.dir_desktop_entry = get_abspath(os.path.join(self.home, '.local', 'share', 'applications'))
            self.dir_themes = get_abspath(os.path.join(self.home, '.themes'))
            self.dir_optional = get_abspath(os.path.join(self.home, '.local', 'opt'))
            self.dir_cache = get_abspath(os.path.join(self.home, '.cache'))
            self.dir_config = get_abspath(os.path.join(self.home, '.config'))
            self.dir_gnupg = get_abspath(os.path.join(self.home, '.gnupg'))
            self.dir_downloads = get_abspath(os.path.join(self.home, 'Downloads'))
            self.file_bashrc = get_abspath(os.path.join(self.home, '.bashrc'))

    def getDirIcons(self, resol: str) -> str:
        """
           resol = Resolução (96x96, 128x128, 256x256, ...)
        Retorna um caminho em: 
           /usr/share/icons/hicolor/resol/apps OU
           ~/.local/share/icons/hicolor/resol/apps
        """
        if self.type_root:
            _path = self.dir_icons.split('/')
            _path[5] = resol
            return "/".join(_path)
        else:
            _path = self.dir_icons.split('/')
            _path[6] = resol
            return "/".join(_path)

    def getDirs(self) -> dict:
        
        return {
            'DIR_BIN': self.dir_bin,
            'DIR_LIB': self.dir_lib,
            'DIR_ICONS': self.dir_icons,
            'DIR_DESKTOP_ENTRY': self.dir_desktop_entry,
            'DIR_THEMES': self.dir_themes,
            'DIR_OPTIONAL': self.dir_optional,
            'DIR_CACHE': self.dir_cache,
            'DIR_CONFIG': self.dir_config,
            'FILE_BASHRC': self.file_bashrc,
        }


class SystemLocalDirsWindows(PathUserDirs):
    """
    Esta classe tem como atributos, diretórios comumente usados por programas no sitema Windows.
    """
    def __init__(self, *, type_root: bool=False):
        super().__init__(type_root)
        self.dir_bin = get_abspath(os.path.join(self.home, 'AppData', 'Local', 'Programs'))
        self.dir_optional = get_abspath(os.path.join(self.home, 'AppData', 'Local', 'Programs'))
        self.dir_gnupg = get_abspath(os.path.join(self.home, '.gnupg'))
        self.dir_cache = get_abspath(os.path.join(self.home, 'AppData', 'LocalLow'))
        self.dir_config = os.path.abspath(os.path.join(self.home, 'AppData', 'Roaming'))
        self.dir_downloads = get_abspath(os.path.join(self.home, 'Downloads'))

    def getDirs(self):
        """
          Retorna um dicionário com diretórios.

          :rtype dict
        """
        return {
            'HOME': self.home,
            'DIR_CACHE': self.dir_cache,
            'DIR_CONFIG': self.dir_config,
            'DIR_BIN': self.dir_bin,
            'DIR_OPTIONAL': self.dir_optional,
            'DIR_GNUPG': self.dir_gnupg,
            }



class SystemLocalDirs(PathUserDirs):
    def __init__(self, *, type_root:bool=False) -> None:
        super().__init__(type_root) 
        self.type_root: bool = type_root
    
        if KERNEL_TYPE == 'Windows':
            self.system_local_dirs: SystemLocalDirsWindows = SystemLocalDirsWindows(type_root=type_root)
        elif KERNEL_TYPE == 'Linux':
            self.system_local_dirs: SystemLocalDirsLinux = SystemLocalDirsLinux(type_root=self.type_root)
        else:
            self.system_local_dirs = None

        if self.system_local_dirs is None:
            sys.exit(1)

        self.system_local_dirs.setDirs()
        self.__temp_dir = None
        self.__temp_file = None

    def setTypeRoot(self, new_type_root: bool) -> None:
        """
          Alterar o atributo type_root para True/False
        inclusive no objeto conf_user_dirs.
        """
        self.type_root = new_type_root
        self.system_local_dirs.type_root = new_type_root
        self.system_local_dirs.setDirs()

    def tempDir(self, *, create=False) -> str:
        """
           Retorna um diretório temporário.
        """
        if self.__temp_dir is None:
            self.__temp_dir = TemporaryDirectory().name
    
        if create:
            mkdir(self.__temp_dir)

        return self.__temp_dir

    def tempFile(self, create=False) -> None:
        if self.__temp_file is None:
            self.__temp_file = NamedTemporaryFile(delete=True).name

        if create:
            touch(self.__temp_file)

        return self.__temp_file
    
    def random_temp_file(self) -> str:
        return NamedTemporaryFile(delete=True).name
    
    def random_temp_dir(self) -> str:
        _random_tmp_dir = TemporaryDirectory().name
        mkdir(_random_tmp_dir)
        return _random_tmp_dir

    def dirHome(self) -> str:
        return self.system_local_dirs.home

    def dirDownloads(self) -> str:
        return self.system_local_dirs.dir_downloads

    def dirBin(self) -> str:
        return self.system_local_dirs.dir_bin

    def dirOptional(self) -> str:
        return self.system_local_dirs.dir_optional

    def dirCache(self) -> str:
        return self.system_local_dirs.dir_cache

    def dirConfig(self) -> str:
        return self.system_local_dirs.dir_config

    def dirDesktopEntry(self) -> str:
        if KERNEL_TYPE != 'Linux':
            return None
        return self.system_local_dirs.dir_desktop_entry

    def dirThemes(self) -> str:
        if KERNEL_TYPE != 'Linux':
            return None
        return self.system_local_dirs.dir_themes

    def dirLib(self) -> str:
        if KERNEL_TYPE != 'Linux':
            return None
        return self.system_local_dirs.dir_lib

    def dirIcons(self) -> str:
        if KERNEL_TYPE != 'Linux':
            return None
        return self.system_local_dirs.dir_icons

    def fileBashRc(self) -> str:
        if KERNEL_TYPE != 'Linux':
            return None
        return self.system_local_dirs.file_bashrc

    def getDirIcons(self, resol: str):
        return self.system_local_dirs.getDirIcons(resol)

    def getDirs(self) -> dict:
       
        return {
                'HOME': self.dirHome(),
                'DIR_DOWNLOADS': self.dirDownloads(),
                'DIR_BIN': self.dirBin(),
                'DIR_LIB': self.dirLib(),
                'DIR_ICONS': self.dirIcons(),
                'DIR_DESKTOP_ENTRY': self.dirDesktopEntry(),
                'DIR_THEMES': self.dirThemes(),
                'DIR_OPTIONAL': self.dirOptional(),
                'DIR_CACHE': self.dirCache(),
                'DIR_CONFIG': self.dirConfig(),
                'FILE_BASHRC': self.fileBashRc(),
        }

    def dirsJson(self):
        """
           Retorna os valores em formato Json.
        
        self.get_dirs(): dict -> self.get_dirs(): json
        """
        return json.dumps(self.getDirs(), indent=4)

    def showDirs(self):
        _dirs = self.getDirs()
        
        for _key in _dirs:
            print(f'{_key}'.ljust(19), end=' ')
            print(_dirs[_key])
    
    def setDirs(self):
        self.system_local_dirs.setDirs()
        

#===============================================================#

class AppLocalDirs(object):
    def __init__(self, *, type_root:bool=False, appname: str = None) -> None:
        super().__init__()
        # Appname é o nome do aplicativo que irá usar esta classe
        # para obter informações de arquivos e diretórios de configuração.
        self.appname = appname
        self.type_root = type_root
        self.system_dirs: SystemLocalDirs = SystemLocalDirs(type_root=type_root)
        
        if self.system_dirs is None:
            print(f'{__class__.__name__} ERRO ... sistema operacional não suportado.')
            exit(1)

        self.system_dirs.setDirs()

    @property
    def appname(self) -> str:
        return self._appname

    @appname.setter
    def appname(self, new_appname: str) -> None:
        self._appname = new_appname

    def set_type_root(self, new_type_root: bool) -> None:
        """
          Alterar o atributo type_root para True/False
        inclusive no objeto conf_user_dirs.
        """
        self.type_root = new_type_root
        self.system_dirs.setTypeRoot(self.type_root)
        self.system_dirs.setDirs()

    def get_dirs(self):
        return {
            'APP_DIR': self.appdir(),
            'APP_DIR_CACHE': self.dircache(),
            'APP_DIR_CONFIG': self.dirconfig(),
        }

    def dircache(self) -> str:
        return get_abspath(os.path.join(self.system_dirs.dirCache(), self.appname))

    def dirconfig(self) -> str:
        return get_abspath(os.path.join(self.system_dirs.dirConfig(), self.appname))

    def appdir(self) -> str:
        return get_abspath(os.path.join(self.system_dirs.dirOptional(), self.appname))

    def file_config(self) -> str:
        """Retorna o caminho de um arquivo .json no diretório de configuração"""
        return get_abspath(os.path.join(self.dirconfig(), f'{self.appname}.json'))

    def fileConfigPath(self) -> FilePath:
        """Retorna uma instância de FilePath para o arquivo self.fileconf()"""
        return FilePath(self.file_config())

    def script(self) -> str:
        return get_abspath(os.path.join(self.system_dirs.dirBin(), self.appname))

    def scriptPath(self) -> FilePath:
        """Retorna uma instância de FilePath para o arquivo self.script()"""
        return FilePath(self.script())

    def icon(self, file_icon: str) -> str:
        """
          Recebe o nome de um arquivo (.png, .jpg, ,svg ...)
        Retorna o caminho onde o icone deve estar no sistema.
        """
        return get_abspath(os.path.join(self.system_dirs.dirIcons(), file_icon))

    def get_dir_icons(self, resol: str) -> str:
        if KERNEL_TYPE == 'Linux':
            return self.system_dirs.getDirIcons(resol)

    def get_temp_dir(self, *, create=False) -> str:
        """
           Retorna um diretório temporário.
        """
        return self.system_dirs.tempDir(create=create)

    def get_temp_file(self, create=False) -> None:
        """Retorna um arquivo temporário."""
        return self.system_dirs.tempFile(create=create)

    def desktop_entry(self, file_desktop) -> str:
        """
        A extensão .desktop é adicionada automáticamente.

        Retorna o caminho completo do arquivo .desktop.
        """
        if file_desktop[-8:] != '.desktop':
            file_desktop += '.desktop'

        return get_abspath(os.path.join(self.system_dirs.dirDesktopEntry(), file_desktop))

    def desktopEntryPath(self, file_desktop: str) -> FilePath:
        """Retorna uma instância de FilePath para o arquivo self.desktop_entry()"""
        return FilePath(self.desktop_entry(file_desktop))

    def show_dirs(self):
        _dirs = self.get_dirs()
        for _key in _dirs:
            print(f'{_key}'.ljust(19), end=' ')
            print(_dirs[_key])

    def create_dirs(self) -> bool:
        """
         Cria os diretórios de configuração.
        """
        if self.type_root:
            return False

        _dirs = self.system_dirs.getDirs()
        _appdirs = self.get_dirs()

        for KEY in _dirs:          
            mkdir(_dirs[KEY])

        for KEY in _appdirs:          
            mkdir(_appdirs[KEY])

#===============================================================#
            

class ExecShellCommand(object):
    """
       Classe para executar comandos shell, apartir da lista 'self.cli'
    """
    
    def __init__(self, cli: list=[]) -> None:
        super().__init__()
        self.cli: list = cli # Lista com comandos shell
        self.isproc: bool = False
        self.returnbool: bool = True
        self.returncode: int = 0
        self.text_exit = None
        self.current_line = None

    @property
    def cli(self) -> list:
        return self._cli

    @cli.setter
    def cli(self, new_cli) -> None:
        if not isinstance(new_cli, list):
            print(f'{__class__.__name__} ERRO os comandos devem ser passados como lista.')
            return
        self._cli = new_cli

    @property
    def isproc(self) -> bool:
        return self._isproc

    @isproc.setter
    def isproc(self, isproc) -> None:
        self._isproc = isproc

    def get_process(self) -> Popen:
        """
           Retorna um objeto do tipo Popen(), com os comandos de self.cli
        se outro processo criando anteriormente por esta classe ainda estiver
        em execução, o retorno será None. 
        """
        if self.cli == []:
            return None

        if self.isproc:
            print(f'{__class__.__name__} outro processo já está em execução, aguarde...')
            return None

        try:
            _proc: Popen = Popen(self.cli, stdin=PIPE, stdout=PIPE, stderr=PIPE, encoding='utf8')
        except Exception as e:
            print(e)
            return None

        self.isproc = True
        return _proc

    def exec(self) -> None:
        """
          Executa um comando e exibe as linhas de saída no stdout.
        """
        proc: Popen = self.get_process()
        if proc is None:
            self.returnbool = False
            self.returncode = 1
            self.text_exit = None
            return

        for line in proc.stdout:
            self.current_line = line
            print(line, end=' ')
            sys.stdout.flush()
        print()
        proc.wait()

        self.returncode = proc.returncode
        self.isproc = False
        if proc.returncode == 0:
            self.returnbool = True
            self.text_exit = proc.communicate()[0]
        else:
            self.returnbool = False
            self.text_exit = proc.stderr
            print(f'EXIT -> {self.returncode}')

        self.cli.clear()

    def exec_silent(self) -> bool:
        """
          Executa um comando sem exibir nada no stdout, apenas ERROS serão exibidos
        no stderr após a execução do comando.
        """
        self.isproc = True
        try:
            proc = subprocess_run(self.cli, text=True, capture_output=True)
        except Exception as e:
            print(e)
            self.returnbool = False
            self.returncode = 1
            self.text_exit = None
            self.isproc = False
            
        self.returncode = proc.returncode
        self.isproc = False
        if proc.returncode == 0:
            self.returnbool = True
            self.text_exit = proc.stdout
        else:
            self.returnbool = False
            self.text_exit = proc.stderr
        self.cli.clear()


class ByteSize(int):
    """
      Classe para mostrar o tamaho de um arquivo (B, KB, MB, GB) de modo legível para humanos.
    """

    # https://qastack.com.br/programming/1392413/calculating-a-directorys-size-using-python
    # 2021-11-13 - 21:12
    
    _kB = 1024
    _suffixes = 'B', 'kB', 'MB', 'GB', 'PB'

    def __new__(cls, *args, **kwargs):
        return super().__new__(cls, *args, **kwargs)

    def __init__(self, *args, **kwargs):
        self.bytes = self.B = int(self)
        self.kilobytes = self.kB = self / self._kB**1
        self.megabytes = self.MB = self / self._kB**2
        self.gigabytes = self.GB = self / self._kB**3
        self.petabytes = self.PB = self / self._kB**4
        *suffixes, last = self._suffixes
        suffix = next((
            suffix
            for suffix in suffixes
            if 1 < getattr(self, suffix) < self._kB
        ), last)
        self.readable = suffix, getattr(self, suffix)

        super().__init__()

    def __str__(self):
        return self.__format__('.2f')

    def __repr__(self):
        return '{}({})'.format(self.__class__.__name__, super().__repr__())

    def __format__(self, format_spec):
        suffix, val = self.readable
        return '{val:{fmt}} {suf}'.format(val=val, fmt=format_spec, suf=suffix)

    def __sub__(self, other):
        return self.__class__(super().__sub__(other))

    def __add__(self, other):
        return self.__class__(super().__add__(other))

    def __mul__(self, other):
        return self.__class__(super().__mul__(other))

    def __rsub__(self, other):
        return self.__class__(super().__sub__(other))

    def __radd__(self, other):
        return self.__class__(super().__add__(other))

    def __rmul__(self, other):
        return self.__class__(super().__rmul__(other))



class FileSize(object):
    """Obeter o tamanho de arquivos e diretórios com python."""
    def __init__(self, path: str):
        self.path: str = path # arquivo/diretório.

    def _get_folder_size(self) -> float:
        _path: Path = Path(self.path)
        total: float = 0.0
        for item in _path.rglob('*'):
            total += os.path.getsize(item)
        return total

    def _get_file_size(self) -> float:
        return float(os.path.getsize(self.path))

    def get_size(self) -> float:
        if os.path.isdir(self.path):
            return self._get_folder_size()
        else:
            return self._get_file_size()

    def human_size(self) -> ByteSize:
        """Retorna o tamaho de um arquivo ou diretório de maneira legivel a humanos."""
        return ByteSize(int(self.get_size()))



class ShellCoreUtils(object):
    """Classe para executar operações básicas de sistemas, como:
    
    - copiar arquivos
    - mover arquivos
    - apagar arquivos
    - criar diretórios
    - criar arquivos.
    - obter o tamanho de arquivos
    - obter a extensão de um arquivo
    - obter o tipo de arquivo via cabeçãlho
    """
    def __init__(self) -> None:
        super().__init__()

    def copy(self, SRC: str, DEST: str) -> bool:
        pass

    def rmdir(self, path: str) -> bool:
        pass

    def mkdir(self, path: str) -> bool:
        pass

    def file_size(self, file: str, human=False) -> float:
        pass

    def get_header_file(self, file) -> str:
        """Retorna o cabeçalho de um arquivo."""
        pass

    def get_extension_file(self, file) -> str:
        """Retorna a extensão de um arquivo."""
        pass

    def get_type_file(self, file: str) -> str:
        """Retorna o tipo de arquivo, com base no cabeçalho"""
        pass

    def device_ismounted(self, device: str) -> bool:
        return device_is_mounted(device)


class PyShCore(ShellCoreUtils):
    """
       Executar operações como copiar, remover, descompactar entre outras usando módulos
    nativos do Python, sem as ferramentas Unix/Linux.
    """

    def __init__(self, *, exec_root: bool=False, verbose=False) -> None:
        super().__init__()
        self._cmd_list = []
        self.returnbool: bool = True
        self.exception_text = None
        self.exception_type = None
        self.verbose = verbose

    def print_text_inline(self, text: str) -> None:
        if not self.verbose:
            return
        print(text, end=' ')
        sys.stdout.flush()

    def _copy_dir(self, src: str, dest: str) -> bool:
        """Método interno para copiar diretórios"""
        try:
            copytree(src, dest, symlinks=True, ignore=None)
        except Exception as e:
            print(__class__.__name__, e)
            self.exception_text = e
            self.exception_type = type(e)
            self.returnbool = False
            return False
        else:
            self.exception_text = None
            self.exception_type = None
            self.returnbool = True
            return True

    def _copy_files(self, src: str, dest: str) -> bool:
        """Método interno para copiar arquivos"""
        try:
            copy(src, dest, follow_symlinks=False)
        except Exception as e:
            print(__class__.__name__, e)
            self.exception_text = e
            self.exception_type = type(e)
            self.returnbool = False
            return False
        else:
            self.exception_text = None
            self.exception_type = None
            self.returnbool = True
            return True

    def copy(self, src: str, dest: str) -> bool:
        """Copia arquivos e diretórios."""
        if os.path.isdir(src):
            return self._copy_dir(src, dest)
        else:
            return self._copy_files(src, dest)

    def mkdir(self, path: str) -> bool:
        """Cria um diretório."""
        try:
            os.makedirs(path)
        except(PermissionError):
            print(f'{__class__.__name__} você não tem permissão de escrita em ... {path}')
            self.exception_type = 'PermissionError'
            self.returnbool = False
            return False
        except(FileExistsError):
            self.exception_type = 'FileExistsError'
            if os.access(path, os.W_OK):
                self.returnbool = True
                return True
            self.returnbool = False
            return False
        except Exception as e:
            print(__class__.__name__, e)
            self.returnbool = False
            self.exception_text = e
            self.exception_type = type(e)
            return False

    def _rmfile(self, path) -> bool:
        """Método interno para remover um arquivo."""
        try:
            os.remove(path)
        except Exception as e:
            print(__class__.__name__, e)
            self.exception_text = e
            self.exception_type = type(e)
            self.returnbool = False
            return False
        else:
            self.exception_text = None
            self.exception_type = None
            self.returnbool = True
            return True

    def _rmdirectory(self, path) -> bool:
        """Método interno para remover um diretório."""
        try:
            rmtree(path)
        except Exception as e:
            print(__class__.__name__, e)
            self.exception_text = e
            self.exception_type = type(e)
            self.returnbool = False
            return False
        else:
            self.exception_text = None
            self.exception_type = None
            self.returnbool = True
            return True

    def _rmlink(self, path) -> bool:
        try:
            os.unlink(path)
        except Exception as e:
            print(__class__.__name__, e)
            self.exception_text = e
            self.exception_type = type(e)
            self.returnbool = False
            return False
        else:
            self.exception_text = None
            self.exception_type = None
            self.returnbool = True
            return True

    def rmdir(self, path: str) -> bool:
        '''
        Remove Arquivos e diretórios.
        '''
        self.print_text_inline(f'Deletando ... {path}')

        if os.path.isdir(path):
            return bool(self._rmdirectory(path))
        elif os.path.islink(path):
            return bool(self._rmlink(path))
        elif os.path.isfile(path):
            return bool(self._rmfile(path))
        elif not os.path.exists(path):
            self.exception_type = 'FileNotFoundError'
            self.exception_text = f'Arquivo ou diretório não encontrado ... {path}'
            self.returnbool = False
            print(self.exception_text)
            return False
        return False

    def file_size(self, path, *, human=False) -> float:
        """
          Retorna o tamanho de um arquivo ou diretório.
        """
        if not human:
            return FileSize(path).get_size()
        return  FileSize(path).human_size()

    def get_header_file(self, file: str) -> str:
        """Retorna o cabeçalho de um arquivo."""
        return None

    def get_extension_file(self, file) -> str:
        """Retorna o tipo de arquivo baseado no cabeçalho"""
        return None

    def get_type_file(self, file: str) -> str:
        """Retorna o tipo de arquivo baseado no cabeçalho"""
        return None


#===============================================================#

shellcore = PyShCore()

#===============================================================#

class ShutilUnpack(object):
    """Descompactar arquivos usando shutil."""
    def __init__(self, file_path:FilePath, *, output_dir=os.getcwd(), format: str):
        
        self.file_path: FilePath = file_path
        self.output_dir:bool = output_dir
        self.returnbool:bool = True # OK/ERRO (True/False)
        self.format: str = format

    def unpack(self) -> bool:
        """
            Recebe um objeto/File para ser descompactado em self.output_dir.
        path_file = Instância da classe File().
        """
   
        try:
            unpack_archive(self.file_path.absolute(), extract_dir=self.output_dir, format=self.format)
        except Exception as e:
            print(e)
            self.returnbool = False
        else:
            self.returnbool = True
        
        return self.returnbool

def unpack(compressed_file: str, *, output_dir: str=os.getcwd(), verbose :bool=True) -> bool:
    """
        Descompacta arquivos.
    compresse_file = caminho absoluto do arquivo a ser descomprimido.
    output_dir     = destino dos dados a serem descomprimidos.
    verbose        = bool
    """

    # Setar o formato de arquivo.
    # FORMATO - TIPO
    #    tar    XZ
    #    tar    bzip2
    #    tar    gzip
    #    zip    Zip
    #
    
    path_file: FilePath = FilePath(compressed_file)
    f_extension = path_file.extension().replace('.', '') # .xz .zip .gz .bz2

    if (f_extension == 'xz') or (f_extension == 'gz') or (f_extension == 'bz2'):
        format_file = 'tar'
    elif (f_extension == 'zip'):
        format_file = 'zip'

    unpack_file = ShutilUnpack(path_file, output_dir=output_dir, format=format_file)
   
    if verbose:
        print(f'[DESCOMPACTANDO] {path_file.name()}', end=' ')
        sys.stdout.flush()
        
    unpack_file.unpack()
    if verbose and unpack_file.returnbool:
        print('OK')
    return unpack_file.returnbool
        
    
#===============================================================#
# Hash utils
#===============================================================#

def get_bytes(data) -> bytes:
    """
        data = arquivo/string/bytes
    converte data para bytes e retorna bytes.
    """
    if isinstance(data, bytes):
        return data

    if isinstance(data, str):
        # Verificar se data é um texto ou um arquivo.
        _bytes = None
        if os.path.isfile(data):
            try:
                with open(data, 'rb') as file:
                    _bytes = file.read()
            except Exception as e:
                print(e)
        else:
            _bytes = str.encode(data)
        return _bytes    



class ShaSumUtils(object):
    def __init__(self) -> None:
        super().__init__()
    
    def _get_bytes(self, data) -> bytes:
        """
            data = arquivo/string/bytes
        converte data para bytes e retorna bytes.
        """
        if isinstance(data, bytes):
            return data

        if isinstance(data, str):
            # Verificar se data é um texto ou um arquivo.
            _bytes = None
            if os.path.isfile(data):
                try:
                    with open(data, 'rb') as file:
                        _bytes = file.read()
                except Exception as e:
                    print(e)
            else:
                _bytes = str.encode(data)
            return _bytes

    def getmd5(self, data) -> str:
        """
        Retorna a hash md5 de data.
        data = arquivo/texto/bytes
        """
        pass

    def getsha1(self, data) -> str:
        """
        Retorna a hash sha1 de data.
        data = arquivo/texto/bytes
        """
        pass
        
    def getsha256(self, data) -> str:
        """
        Retorna a hash sha256sum de data.
        data = arquivo/texto/bytes
        """
        pass

    def getsha512(self, data) -> str:
        """
        Retorna a hash sha512 de data.
        data = arquivo/texto/bytes
        """
        pass

    def check_md5(self, data, md5_string: str) -> bool:
        pass

    def check_sha1(self, data, sha1_string: str) -> bool:
        pass

    def check_sha256(self, data, sha256_string: str) -> bool:
        pass

    def check_sha512(self, data, sha512_string: str) -> bool:
        pass


class CheckSums(ShaSumUtils):
    def __init__(self) -> None:
        super().__init__()

    def getmd5(self, data) -> str:
        """
        Retorna a hash md5 de data.
        data = arquivo/texto/bytes
        """
        data_bytes = get_bytes(data)
        if data_bytes is None:
            return None
        return hashlib.md5(data_bytes).hexdigest()

    def getsha1(self, data) -> str:
        """
        Retorna a hash sha1 de data.
        data = arquivo/texto/bytes
        """
        data_bytes = get_bytes(data)
        if data_bytes is None:
            return None
        return hashlib.sha1(data_bytes).hexdigest()
        
    def getsha256(self, data) -> str:
        """
        Retorna a hash sha256sum de data.
        data = arquivo/texto/bytes
        """
        data_bytes = get_bytes(data)
        if data_bytes is None:
            return None
        return hashlib.sha256(data_bytes).hexdigest()

    def getsha512(self, data) -> str:
        """
        Retorna a hash sha512 de data.
        data = arquivo/texto/bytes
        """
        data_bytes = get_bytes(data)
        if data_bytes is None:
            return None
        return hashlib.sha512(data_bytes).hexdigest()
    
    def check_md5(self, data, md5_string: str) -> bool:
        if len(md5_string) != 32:
            print(f'{__class__.__name__} ERRO hash do tipo md5 deve ter 32 caracteres.')
            return False

        if self.getmd5(data) == md5_string:
            return True

        print(f'{__class__.__name__} FALHA')
        return False

    def check_sha1(self, data, sha1_string: str) -> bool:
        if len(sha1_string) != 40:
            print(f'{__class__.__name__} ERRO hash do tipo sha1 deve ter 40 caracteres.')
            return False

        if self.getsha1(data) == sha1_string:
            return True

        print(f'{__class__.__name__} FALHA')
        return False

    def check_sha256(self, data, sha256_string: str) -> bool:
        if len(sha256_string) != 64:
            print(f'{__class__.__name__} ERRO hash do tipo sha256 deve ter 64 caracteres.')
            return False

        if self.getsha256(data) == sha256_string:
            return True

        print(f'{__class__.__name__} FALHA')
        return False

    def check_sha512(self, data, sha512_string: str) -> bool:
        if len(sha512_string) != 128:
            print(f'{__class__.__name__} ERRO hash do tipo sha512 deve ter 128 caracteres.')
            return False

        if self.getsha512(data) == sha512_string:
            return True

        print(f'{__class__.__name__} FALHA')
        return False



#===============================================================#

shasum: CheckSums = CheckSums()

#===============================================================#

class GpgUtils(object):
    def __init__(self) -> None:
        super().__init__()
        
    def importKeyFile(self, file) -> bool:
        pass

    def importKeyData(self, file) -> bool:
        pass

    def verifyFile(self, *, file: str, sign_file: str) -> bool:
        pass

    
class GpgLinux(GpgUtils):
    def __init__(self) -> None:
        super().__init__()

    def importKeyFile(self, file) -> bool:
        proc = ExecShellCommand(['gpg', '--import', file])
        proc.exec_silent()
        return proc.returnbool

    def verifyFile(self, *, file: str, sig_file: str) -> bool:
        #if os.system(f'gpg --verify {sig_file} {file}') == 0:
        #   return True
        #return False
        proc = ExecShellCommand(['gpg', '--verify', sig_file, file])
        proc.exec_silent()
        return proc.returnbool
   
    
gpg_utils = GpgLinux()


class Downloader(object):
	def __init__(self):
		super().__init__()
		self._terminal_width = 80

	def set_terminal_width(self) -> None:
		self._terminal_width = self.get_terminal_width()

	def get_terminal_width(self) -> int:
		try:
			width = int(os.get_terminal_size()[0])
		except:
			width = 80
		return width

	def clean_line(self):
		print(' ' * self.get_terminal_width(), end='\r')

	def get_headers(self, url: str) -> dict:
		"""Retorna informações de um arquivo online, se for possivel."""

		info = {}

		try:
			req = urllib.request.urlopen(url)
		except Exception as err:
			print(f'{__class__.__name__} {err}')
		else:
			info = req.info()
		finally:
			return info

	def download(self, url: str, output_file: str) -> bool:
		print(f'Baixando ... {url}')
		sys.stdout.flush()
		try:
			urllib.request.urlretrieve(url, output_file)
		except Exception as e:
			print(e)
			return False
		else:
			return True



# Classe de uso interno.
class RequestDownloaderWithTqdm(Downloader):
    def __init__(self):
        super().__init__()

    def download(self, url: str, filename: str) -> bool:

        file_path: FilePath = FilePath(filename)

        r = requests.get(url, stream=True)
        try:
            file_size = int(r.headers['Content-Length'])
        except:
            file_size = int(0)

        chunk = 1
        chunk_size = 1024
        num_bars = int(file_size / chunk_size)
        self.clean_line()

        try:
            with open(file_path.absolute(), 'wb') as fp:
                for chunk in tqdm.tqdm(
                    r.iter_content(chunk_size=chunk_size),
                    total=num_bars,
                    unit='KB',
                    desc=file_path.part_name(35),
                    leave=True # progressbar stays
                    ):
                    fp.write(chunk)

        except Exception as e:
            print(e)
            return False
        else:
            return True



# Classe de uso interno.
class HtmlUtils(object):
	def __init__(self):
		super().__init__()

	def is_url(self, url: str) -> bool:
		"""Verifica se uma string é do tipo url"""

		RegExp = re.compile(r'^http:|^ftp:|^https|^www')
		if RegExp.findall(url) == []:
			print(f'{__class__.__name__} ERRO url inválida ... {url}')
			return False
		return True

	def get_html_lines(self, url: str) -> list:
		'''
		Baixa o html de uma página e retorna o contéudo em forma de lista.
		'''
		return self.get_html_page(url).split('\n')

	def get_html_page(self, url: str) -> str:
		'''
		Faz o request de uma página e retorna o conteúdo.
		'''
		if not self.is_url(url):
			return []

		try:
			req = urllib.request.Request(url, data=None)
			html = urllib.request.urlopen(req)
		except Exception as err:
			print(f'{__class__.__name__} {err}')
			return None
		else:
			return html.read().decode('utf-8')

	def get_html_links(self, url: str) -> list:
		'''
		Retornar uma lista com todos os links encontrados em um url/html.
		Requer o módulo bs4.
		'''
		links = []
		html = self.get_html_page(url)
		if html is None:
			return []

		soup = BeautifulSoup(html, 'html.parser')
		for link in soup.findAll('a'):
			l = link.get('href')
			links.append(l)
		return links



class DownloaderRequest(Downloader):

	def __init__(self, *, verbose=True):
		super().__init__()
		self.verbose = verbose
		self._downloader = RequestDownloaderWithTqdm()
		self._html_utils: HtmlUtils = HtmlUtils()

	def download_file(self, url: str, file: str) -> bool:
		if os.path.isfile(file):
			print(f' [PULANDO] -> {file}')
			return True

		sys.stdout.flush()
		out: bool = self._downloader.download(url, file)
		sys.stdout.flush()
		return out 

	def get_html_lines(self, url: str) -> list:
		return self._html_utils.get_html_lines(url)

	def get_html_page(self, url: str) -> list:
		return self._html_utils.get_html_page(url)

	def get_html_links(self, url: str) -> list:
		return self._html_utils.get_html_links(url)

	def is_url(self, url: str) -> bool:
		return self._html_utils.is_url(url)



def main():
    cfg: SystemLocalDirs = SystemLocalDirs()
    cfg.showDirs()
    


if __name__ == '__main__':
    main()
