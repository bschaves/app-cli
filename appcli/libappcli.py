#!/usr/bin/env python3

import sys
import os

from appcli.apps import Package

try:
    from appcli.common.colors import *
    from appcli._version import __version__
    from appcli.common.utils import (print_line, question)
    from appcli.common.colors import Color
    from appcli import apps
    from appcli.apps import (
                            GetApps, 
                            Package, 
                            download_manager, 
                            appcli_conf_dirs,
                            )

    from appcli.common.apps_conf import (
                            FilePath,
                            FileJson,
                            FileReader,
                            ExecShellCommand,
                            SystemLocalDirs,
                            shasum,
                            unpack,
                            shellcore,
                            KERNEL_TYPE,
                            HOME
                            )
    
except Exception as e:
    print(e)
    sys.exit(1)


#===========================================================#
# Classes para executar comandos como: 
# Instalar | Baixar | Desistalar os pacotes
#===========================================================#

class CommandApps(object):
    def __init__(self, app: Package) -> None:
        self.app: Package = app
        self.dir_project = None

    @property
    def dir_project(self) -> str:
        return self._dir_project

    @dir_project.setter
    def dir_project(self, new_dir: str):
        if new_dir is None:
            self._dir_project = None
            return
        if os.path.isdir(new_dir):
            self._dir_project = new_dir
    def execute(self) -> bool:
        pass
class CommandDownloadApps(CommandApps):
    def __init__(self, app: Package) -> None:
        super().__init__(app)

    def execute(self) -> bool:
        return self.app.download()


class CommandInstallApps(CommandApps):
    def __init__(self, app: Package) -> None:
        super().__init__(app)
        self.app = app

    def execute(self) -> bool:
        # Verificar se o app já está instalado.
        print(f'Instalando: {self.app.appname}')
        return self.app.install()

class CommandUninstallApps(CommandApps):
    def __init__(self, app: Package) -> None:
        super().__init__(app)
        self.app = app

    def execute(self) -> bool:
        print(f'Desinstalando: {self.app.appname}')
        return self.app.uninstall()
    
class CommandCheckSumApps(CommandApps):
    def __init__(self, app: Package) -> None:
        super().__init__(app)
        self.dir_project = None

    @property
    def dir_project(self) -> str:
        return self._dir_project

    @dir_project.setter
    def dir_project(self, new_dir: str):
        if new_dir is None:
            self._dir_project = None
            return
        if os.path.isdir(new_dir):
            self._dir_project = new_dir

    def execute(self) -> bool:
        if self.app.pkg_file_path() is None:
            return False

        print(f'[CHECKSUM] {self.app.pkg_file_path().basename()} ->', end=' ')
        if self.app.pkg_hash_value is None:
            print(f'{Color.RED}[PULANDO] hash não definida.{Color.RESET}')
            return True
        
        if shasum.check_sha256(self.app.pkg_file_abspath(), self.app.pkg_hash_value):
            print(f'OK')
            return True
        return False

class CreateCheckSums(object):
    def __init__(self) -> None:
        self._packages_list: list = []
        self._aproved_list: list = []

        self.dir_project = None

    @property
    def dir_project(self) -> str:
        return self._dir_project

    @dir_project.setter
    def dir_project(self, new_dir: str):
        if new_dir is None:
            self._dir_project = None
            return
        if os.path.isdir(new_dir):
            self._dir_project = new_dir

    def add_command_check_sum(self, command: CommandCheckSumApps):
        """Inclui pacotes na lista de verificação"""
        self._packages_list.append(command)

    def run_check_sums(self):
        
        command: CommandApps = None
        for command in self._packages_list:
            if command.app.pkg_file_path() is None:
                continue
            if command.execute():
                self._aproved_list.append(command)
        

class CreateCommands(object):
    def __init__(self) -> None:
        self._commands_list: list = []
        self.dir_project = None
    @property
    def dir_project(self) -> str:
        return self._dir_project

    @dir_project.setter
    def dir_project(self, new_dir: str):
        if new_dir is None:
            self._dir_project = None
            return
        if os.path.isdir(new_dir):
            self._dir_project = new_dir

    def add_command(self, command: CommandApps):

        self._commands_list.append(command)

    def run_commands(self):
        command: CommandApps = None
        for command in self._commands_list:
            command.app.dir_project = self.dir_project
            command.execute()
            


class PkgsManager(GetApps):
    def __init__(self) -> None:
        super().__init__()
        # Nomes/strings de todos os pacotes recebidos pela linha de comando.
        self.__valid_names = []
        self.download_only = False
        self.assume_yes = False       
        self.dir_project = None

    @property
    def dir_project(self) -> str:
        return self._dir_project

    @dir_project.setter
    def dir_project(self, new_dir: str):
        if new_dir is None:
            return
        if os.path.isdir(new_dir):
            self._dir_project = new_dir

    def apps_to_dict(self) -> dict:
        """
           Retorna um dicionário
        KEY/NOME  VALUE/PKG

        """
        _pkgs = {}
        for pkg in self.get_apps():
            _pkgs.update({pkg.appname: pkg})
        return _pkgs
        
    def get_pkgs_from_names(self, names: list) -> list:
        """
            Retorna uma lista de objetos/Packages com base nos nomes.
        """
        apps: list = []
        valid_names: list = []
        invalid_names: list = []
        for name in names:
            add = False
            for appname in self.get_apps_names():
                if appname == name:
                    valid_names.append(name)
                    add = True

            if not add:
                invalid_names.append(name)

        if invalid_names != []:
            print(f'{__class__.__name__} ERRO os seguinte(s) pacote(s) são {RED}INVÁLIDOS:{RESET}', end=' ')
            for n in invalid_names:
                print(n, end=' ')
            print()

        self.__valid_names = valid_names
        for pkg in self.get_apps():
            if pkg.appname in valid_names:
                apps.append(pkg)
        return apps

    def list_apps(self):
        """Mostra o nome de todos os apps disponíveis para instalação."""
        for name in self.get_apps_names():
            print(name)

    def list_info(self, name: str):
        """Exibe informações de um pacote com base no nome."""
        pkg: Package = self.get_app_from_name(name)
        if pkg is None:
            print(f'{__class__.__name__} ERRO o app informado é inválido.')
            return
        print(pkg.get_info_json())

    def download_apps(self, app_list: list):
        """Recebe uma lista de instâncias da classe Package, e faz o download dos pacotes."""
        
        # Criar comando para Downloads
        create_commands = CreateCommands()
        create_commands.dir_project = self.dir_project
        app: Package = None
        for app in app_list:
            app.dir_project = self.dir_project
            create_commands.add_command(CommandDownloadApps(app))

        print(f"Baixando {len(create_commands._commands_list)} pacote(s)")
        create_commands.run_commands()

    def install_apps(self, names: list):
        """
           Recebe uma lista de nomes/strings dos pacotes, converte cada string em seus
        respectivos pacotes, e efetua a instalação dos pacotes.
        """
        
        apps: list = self.get_pkgs_from_names(names) # Obter lista com instâncias de Package.
        self.download_apps(apps)
        if self.download_only:
            print('Feito somente download')
            return True

        # Verificar integridade
        print_line('-')
        
        create_command_check_sum = CreateCheckSums()
        create_command_check_sum
        _app: Package = None
        for _app in apps:
            _app.dir_project = self.dir_project
            create_command_check_sum.add_command_check_sum(CommandCheckSumApps(_app))

        create_command_check_sum.run_check_sums()
        
        # Usar a lista de apps que tiveram as respectivas hashs verificadas para 
        # Criar comandos de instalação
        create_command_install = CreateCommands()
        create_command_check_sum.dir_project = self.dir_project
        print_line()
        print(f'Instalando {len(create_command_check_sum._aproved_list)} aplicativo(s):\n')
        _cmd: CommandCheckSumApps = None
        for _cmd in create_command_check_sum._aproved_list:
            create_command_install.add_command(CommandInstallApps(_cmd.app))
            print(f'{_cmd.app.appname}', end='        ')
        print()
        create_command_install.run_commands()

    def uninstall_apps(self, names):
        apps: list = self.get_pkgs_from_names(names)
        print_line('-')

        create_command_uninstall = CreateCommands()
        create_command_uninstall.dir_project = self.dir_project
        for app in apps:
            create_command_uninstall.add_command(CommandUninstallApps(app))

        create_command_uninstall.run_commands()

    def clean_package_cache(self, app_name: str) -> None:
        
        if self.get_pkgs_from_names([app_name]) == []:
            return

        app: Package = self.get_pkgs_from_names([app_name])[0]
        if not self.assume_yes:
            if not question(f'Deseja apagar os arquivos em cache de ... {app.appname}'):
                return
        print(f'Apagando cache de ... {app.appname}')
        app.clean_cache()

    def clean_downloads_cache(self):
        if not self.assume_yes:
            if not question(f'Deseja apagar os arquivos em cache de => {appcli_conf_dirs.dircache()}'):
                return
            
        shellcore.rmdir(appcli_conf_dirs.dircache())


class AppcliOnlineManager(object):
    def __init__(self) -> None:
        # https://gitlab.com/bschaves/app-cli/-/archive/V0.3.7/app-cli-V0.3.7.zip
        self.local_dirs = SystemLocalDirs(type_root=False)
        self.branch = 'V0.3.7'
        self.online_url_file_version = f'https://gitlab.com/bschaves/app-cli/-/raw/{self.branch}/appcli/_version.py?ref_type=heads&inline=false'
        self.online_url_appcli_zipfile = f'https://gitlab.com/bschaves/app-cli/-/archive/{self.branch}/app-cli-V0.3.7.zip'
        
        self.tempfile_version = self.local_dirs.random_temp_file()
        self.local_tempdir = self.local_dirs.random_temp_dir()
        self.appcli_zipfile_name = f'app-cli-{self.branch}.zip'
        self.abspath_file_appcli = os.path.join(self.local_tempdir, self.appcli_zipfile_name)

        self.output_bool: bool = False

    def download_appcli_zip_file(self):
        self.output_bool = download_manager.download_file(self.online_url_appcli_zipfile, self.abspath_file_appcli)

    def get_value_online_version(self) -> str:
        # Baixar o arquivo _version.py no repositório.
        download_manager.download_file(self.online_url_file_version, self.tempfile_version)

        # Ler o conteúdo do arquivo para filtrar a versão
        f_path = FilePath(self.tempfile_version)
        f_reader = FileReader(f_path)
        online_version = f_reader.find_text('__version__', max_count=1)[0].replace("'", "").replace("\n", "").replace(" ", "")
        shellcore.rmdir(f_path.absolute())
        return online_version.split('=')[1]


class AppcliLocalManager(Package):
    def __init__(self, *, type_root=False, appname='appcli', assume_yes=False):
        super().__init__(type_root=type_root, appname=appname) 
        self.assume_yes = assume_yes
        self.online_manager = AppcliOnlineManager()
        
    def check_uptade(self):
        online_version = self.online_manager.get_value_online_version()
        if not self.assume_yes:
            if not question(f'Deseja atualizar para versão ... {GREEN}{online_version}{RESET}'):
                return
        self.online_install_appcli()

    def _install_on_windows(self):
        os.system(f'{sys.executable} -m pip install -r requirements.txt')
        os.system(f'{sys.executable} setup.py install')

    def _install_on_linux(self):
    
        ExecShellCommand(['chmod', '+x', 'configure.py']).exec()
        ExecShellCommand(['./configure.py']).exec()

    def online_install_appcli(self) -> bool:

        # Limpar o cache local
        _unpack_dir = self.online_manager.local_tempdir
        if os.path.isfile(self.online_manager.abspath_file_appcli):
            shellcore.rmdir(self.online_manager.abspath_file_appcli)
        
        # Baixar o pacote do repositório
        self.online_manager.download_appcli_zip_file()
        if not self.online_manager.output_bool:
            return
        
        unpack(self.online_manager.abspath_file_appcli, output_dir=_unpack_dir)
        os.chdir(_unpack_dir)
        for _dir in os.listdir('.'):
            if ('app-cli' in _dir) and (os.path.isdir(_dir)):
                os.chdir(_dir)
        
        if KERNEL_TYPE == 'Linux':
            self._install_on_linux()
        elif KERNEL_TYPE == 'Windows':
            self._install_on_windows()
        else:
            pass
        


        