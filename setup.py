#!/usr/bin/env python3


"""
REQUERIMENTOS:

apps_conf >= v0.1.3 
cmdlib >= 0.2.3
python-magic
tqdm
requests
bs4


pip3 install 'https://gitlab.com/bschaves/app-cli/-/archive/main/app-cli-main.zip' --user

"""
import os
import sys

try:
	from setuptools import setup
except Exception as e:
	print(e)
	sys.exit(1)

file_setup = os.path.abspath(os.path.realpath(__file__))
dir_of_project = os.path.dirname(file_setup)

sys.path.insert(0, dir_of_project)

try:
	from appcli._version import (
							__version__,
							__author__,
							__repo__,
							__download_file__,
							)
except Exception as e:
	print(e)
	sys.exit(1)


DESCRIPTION = 'Script para facilitar a instalação de aplicativos em distribuições Linux.'
LONG_DESCRIPTION = 'Script para facilitar a instalação de aplicativos em distribuições Linux.'

setup(
	name='appcli',
	version=__version__,
	description=DESCRIPTION,
	long_description=LONG_DESCRIPTION,
	author=__author__,
	author_email='brunodasill@gmail.com',
	license='MIT',
	packages=['appcli', 'appcli.common'],
	zip_safe=False,
	url='https://gitlab.com/bschaves/appcli',
	project_urls = {
		'Código fonte': __repo__,
		'Download': __download_file__,
	},
)


