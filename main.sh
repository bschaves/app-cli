#!/usr/bin/env bash
#
#
# run appcli
# versão: 2025-02-08
#

readonly _this_file=$(readlink -f "$0")
readonly _this_dir=$(dirname "$_this_file")
readonly PREFIX='local-venv'
readonly DIR_VENV="${HOME}/${PREFIX}"

function main(){
	#echo -e "VENV: $DIR_VENV | PATH: ${DIR_VENV}/bin/python3"
	source "${DIR_VENV}/bin/activate" || return 1
	"${DIR_VENV}/bin/python3" "${_this_dir}/appcli/__main__.py" "$@"
}

main "$@"